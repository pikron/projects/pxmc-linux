#ifndef SUI_DTREEMEM_H
#define SUI_DTREEMEM_H

#include <ul_utdefs.h>
#include "sui_dtree.h"
#include <ul_gavl.h>
#include <ul_gavlcust.h>

extern const char *sui_dtreemem_dll_search_path;


typedef char *sui_dtree_memname_key_t;

/**
 * struct sui_dtree_memdir_t - Ancestor of &sui_dtree_dir_t which containing &sui_dtree_memnode_t GAVL list .
 * @name_root:       GAVL with children of type &sui_dtree_memnode_t
 * @dir:             base struct (Container_of technology). Containing dir needs it.
 * @dll_search_path: If it is not NULL, &dtree_mem tries to find
 * Header:           sui_dtreemem.h
 */
typedef struct sui_dtree_memdir_t {
    sui_dtree_dir_t dir;
    gavl_cust_root_field_t name_root;
} sui_dtree_memdir_t;

#define SUI_DTREE_ISDIR   1
#define SUI_DTREE_ISDINFO 2

//struct sui_dtree_memnode_t;
//typedef int sui_dtree_memnode_destructor_t(struct sui_dtree_memnode_t *node);

/**
 * struct sui_dtree_memnode_t - structure representing single node in memtree.
 *                              Node can contain %dinfo or %directory (&sui_dtree_dir_t).
 * @name:       structure neccessary for storing node in GAVL tree, is NULL for subindicies
 * @node_type:  type of node contens (dir or dinfo)
 * @name_node:  the structure can be stored in GAVL tree thanks to that field
 * @ptr:        pointer to dinfo or directory that this node contains.
 * @dll_handle: if memnode is one imported from DLL, DLLs handle is stored here. (else it is 0)
 * Header:    sui_dtreemem.h.h
 */
 /*
 * @dll_working_thread: if DLL has working thread, thread's structure is here. (else it is 0)
 */
 typedef struct sui_dtree_memnode_t {
    char *name;
    int node_type;
    gavl_node_t name_node;
    union {
        sui_dinfo_t *datai;
        sui_dtree_dir_t *dir;
    } ptr;
    void *dll_handle; /* if node comes from DLL */
} sui_dtree_memnode_t;
    //sui_dtree_memnode_destructor_t destructor;
    //pthread_t dll_working_thread;

/*
    GAVL_CUST_NODE_INT_DEC( 
        prefix, 
        root_type, 
        node_type, 
        key_type, 
        gavl_cust_root_field_t field in root_type, 
        gavl_node_t field in node_type, 
        key field in node_type, 
        key_cmp_fnc
    )
*/
GAVL_CUST_NODE_INT_DEC(sui_dtree_mem, sui_dtree_memdir_t, sui_dtree_memnode_t, sui_dtree_memname_key_t,
	                    name_root, name_node, name, sui_dtpathcmp)

void sui_dtree_memnode_init(sui_dtree_memnode_t *node, char *name, int type);
void sui_dtree_memnode_destroy(sui_dtree_memnode_t *node);

void sui_dtree_memdir_init(sui_dtree_memdir_t *dir);
void sui_dtree_memdir_destroy(sui_dtree_memdir_t *dir);
int sui_dtree_mem_lookup(sui_dtree_dir_t *from_dir, const char *path, int *consumed,
                     sui_dtree_dir_t **found_dir, sui_dinfo_t **datai);

#endif
