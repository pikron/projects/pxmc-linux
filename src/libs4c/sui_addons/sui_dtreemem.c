#include <stdarg.h>
#include <string.h>
#include <dlfcn.h>

#include "ul_log.h"
#include "ul_dbuff.h"
#include "sui_dtreemem.h"

//======================== logging support ==========================

extern UL_LOG_CUST(ulogd_dtreemem);

//=================================================================

// where DLLs should be searched for
const char *sui_dtreemem_dll_search_path = NULL;

//------------------------------------------------------------

/**
 * sui_cmp_terminated - compares two strings terminated either by '\0' or by %terminator.
 *                 Usefull when one works with the path names.
 * @pa: first string
 * @pb: second string
 * @terminators: aditional terminators(\0 stil terminates string too), that indicates end of string
 * Return: the same value like libc strcmp does.
 * File: sui_dtreemem.c
 */
int sui_cmp_terminated(const char *pa, const char *pb, const char *terminators)
{
    const char *a=pa;
    const char *b=pb;
    char ca, cb;
    while(1){
        ca=*(a++);
        cb=*(b++);
        if(strchr(terminators, ca) != NULL)
            ca=0;
        if(strchr(terminators, cb) != NULL)
            cb=0;
        if(!ca){
            if(!cb)
                return 0;
            else
                return 1;
        }
        if(!cb)
            return -1;
        if (ca>cb) return 1;
        if (ca<cb) return -1;
    }
}

//------------------------------------------------------------

int sui_dtpathcmp(const sui_dtree_memname_key_t *pa, const sui_dtree_memname_key_t *pb)
{
    /*
    int ret = vca_cmp_terminated(*pa, *pb, '/');
    ul_logdeb("comparing '%s' and '%s' - ret: %i\n", *pa, *pb, ret);
    return ret;
    */
    return sui_cmp_terminated(*pa, *pb, "/");
}


GAVL_CUST_NODE_INT_IMP(sui_dtree_mem, sui_dtree_memdir_t, sui_dtree_memnode_t, sui_dtree_memname_key_t,
	                    name_root, name_node, name, sui_dtpathcmp)

//------------------------------------------------------------
void sui_dtree_memnode_init(sui_dtree_memnode_t *node, char *_name, int _type)
{
    node->name = _name;
    node->node_type = _type;
    node->ptr.datai = NULL;
    node->dll_handle = 0;
    //node->dll_working_thread = 0;
}

void sui_dtree_memnode_destroy(sui_dtree_memnode_t *node)
{
    if(node->dll_handle) {
        void (*cleanup_module)();
        cleanup_module = dlsym(node->dll_handle, "cleanup_module");
        if(!cleanup_module) {
            char *error = dlerror();
            ul_logerr("ERROR loading symbol 'cleanup_module' from DLL: '%s'\n", error);
        }
        else {
            ul_logdeb("Calling cleanup_module() for node '%s'\n", node->name);
            cleanup_module();
        }
        dlclose(node->dll_handle);
    }
}

//------------------------------------------------------------
void sui_dtree_memdir_init(sui_dtree_memdir_t *dir)
{
    if(!dir) return;
    dir->dir.lookup = sui_dtree_mem_lookup;
}
void sui_dtree_memdir_destroy(sui_dtree_memdir_t *dir)
{
    sui_dtree_memdir_t *root = dir;
    sui_dtree_memnode_t *node;
    while((node = sui_dtree_mem_cut_first(root)) != NULL) {
        // for now all dirs are static. (no one is allocated using malloc)
        sui_dtree_memnode_destroy(node);
        /*
        if(node->destructor) node->destructor(node);
        if(node->node_type & SUI_DTREE_ALLOCATED_FLAG) {
            free(node);
        }
        */
    }
}

//------------------------------------------------------------
/**
 * sui_dtree_mem_lookup - Find dinfo in the named dinfo database
 * @from_dir: the directory to start from
 * @path:   path from directory to dinfo or directory
 * @consumed: pointer to location for numeber of consumed characters from path
 * @found_dir: the optional pointer to space that would hold pointer to directory of found dinfo
 * @datai:  optional pointer to store the found dinfo
 *
 * Return Value: SUI_DTREE_FOUND, SUI_DTREE_DIR, SUI_DTREE_NOPATH, SUI_DTREE_ERROR
 * File: sui_dtreemem.c
 */
int sui_dtree_mem_lookup(sui_dtree_dir_t *from_dir, const char *path, int *consumed,
                     sui_dtree_dir_t **found_dir, sui_dinfo_t **datai)
{
    sui_dtree_memnode_t *memnode;
    const char *p=path;
    sui_dtree_memdir_t *dir;

    if(!from_dir) {
        ul_logerr("%s(): prameter from_dir IS NULL !\n", __PRETTY_FUNCTION__);
        return SUI_DTREE_ERROR;
    }
    dir = UL_CONTAINEROF(from_dir, sui_dtree_memdir_t, dir);
    if(consumed) *consumed = 0;
    ul_logdeb("Looking for '%s'\n", path);
    memnode=sui_dtree_mem_find(dir,(char**)&p);
    if(!memnode) {
        //dinfo can be in DLL, try to load DLL
	void* handle;
        ul_dbuff_t dll_name;
        ul_dbuff_t dll_filename;
        const char *pc = path;
        ul_dbuff_init(&dll_name, 0);
        ul_dbuff_init(&dll_filename, 0);

        while(*pc && *pc!='/') ul_dbuff_append_byte(&dll_name, (unsigned char)*pc++);
        ul_dbuff_append_byte(&dll_name, 0);
        ul_dbuff_strcpy(&dll_filename, sui_dtreemem_dll_search_path);
        ul_dbuff_strcat(&dll_filename, "/");
        ul_dbuff_strcat(&dll_filename, "lib");
        ul_dbuff_strcat(&dll_filename, dll_name.data);
        ul_dbuff_strcat(&dll_filename, ".so");
        //ul_logdeb("DLL search path: '%s'\n", sui_dtreemem_dll_search_path);
        ul_logdeb("Trying DLL '%s' ... ", dll_filename.data);
        handle = dlopen(dll_filename.data, RTLD_LAZY);
        if(!handle) {
            char *error = dlerror();
            ul_loglev(UL_LOGL_DEB|UL_LOGL_CONT, "ERROR - %s\n", error);
            ul_logdeb("This would not be ERROR if dinfo does not exist neither in DLL nor in dtree.\n");
            return SUI_DTREE_NOPATH;
        }
        else {
            int ret;
            sui_dtree_memnode_t* (*get_dtreemem_node)();
            int (*init_module)();
            ul_loglev(UL_LOGL_DEB|UL_LOGL_CONT, "OK\n");

            init_module = dlsym(handle, "init_module");
            if(!init_module) {
                char *error = dlerror();
                ul_logerr("ERROR loading symbol 'init_module' from DLL: '%s'\n", error);
                return SUI_DTREE_ERROR;
            }
            ul_logdeb("Calling init_module() for '%s'\n", dll_name.data);
            ret = (*init_module)();
            ul_loginf("%s::init_module(): %s\n", dll_name.data, ret? "ERROR": "OK");

            get_dtreemem_node = dlsym(handle, "get_dtreemem_node");
            if(!get_dtreemem_node) {
                char *error = dlerror();
                ul_logerr("ERROR loading symbol 'get_dtreemem_node' from DLL: '%s'\n", error);
                return SUI_DTREE_NOPATH;
            }

            memnode = (*get_dtreemem_node)();
            if(!memnode) {
                return SUI_DTREE_NOPATH;
            }
            if(!memnode->name) {
                ul_logerr("ERROR loaded node has no name\n");
                return SUI_DTREE_ERROR;
            }
            if(strcmp(memnode->name, dll_name.data)) {
                ul_logerr("ERROR loaded node has diferent name '%s' than library '%s'\n", memnode->name, dll_name.data);
                return SUI_DTREE_ERROR;
            }
            memnode->dll_handle = handle; // needed for dlclose()
            sui_dtree_mem_insert(dir, memnode);
            ul_loginf("Node '%s' added to dir\n", memnode->name);
            //ul_logdeb("DLL symbols loaded OK\n");
        }
    }

    if(consumed) {
        *consumed = strlen(memnode->name);
        while(path[*consumed] == '/') (*consumed)++; // skip path delimiter
    }
    if(memnode->node_type==SUI_DTREE_ISDIR){
        if(found_dir) *found_dir = memnode->ptr.dir;
        return SUI_DTREE_DIR;
    }
    if(memnode->node_type!=SUI_DTREE_ISDINFO) {
        return SUI_DTREE_ERROR;
    }
    if(found_dir) {
        *found_dir=from_dir;
    }
    if(datai) *datai=memnode->ptr.datai;
    return SUI_DTREE_FOUND;
}

