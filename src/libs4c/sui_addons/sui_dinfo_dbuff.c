#ifndef __RTL__
 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>

#else /*__RTL__*/

#include <rtl.h>
#include <string.h>
#include <signal.h>
#include <posix/unistd.h>

#endif /*__RTL__*/
/*
#include "sui_event.h"
#include "sui_dinfo.h"
#include "ul_evcbase.h"
#include "ul_utmalloc.h"
*/
#include "ul_log.h"
#include "sui_dinfo_dbuff.h"

//======================== logging support ==========================

extern UL_LOG_CUST(ulogd_suidinfo);

//==================================================================
//               Data connection processing 
//==================================================================
static int sui_dbuff_rdval(sui_dinfo_t *di, long idx, void *buf)
{
    ul_dbuff_t *dbuf = (ul_dbuff_t*)buf;
    ul_dbuff_t *db = (ul_dbuff_t*)di->ptr;
    if(di->idxsize && (idx >= di->idxsize)) return SUI_RET_EINDX;
    ul_dbuff_cpy(dbuf, db->data, db->len);
    return SUI_RET_OK;
}

static int sui_dbuff_wrval(sui_dinfo_t *di, long idx, const void *buf)
{
    ul_dbuff_t *dbuf = (ul_dbuff_t*)buf;
    ul_dbuff_t *db = (ul_dbuff_t*)di->ptr;
    if(di->idxsize && (idx >= di->idxsize)) return SUI_RET_EINDX;
    ul_dbuff_cpy(db, dbuf->data, dbuf->len);
    sui_dinfo_changed(di);
    return SUI_RET_OK;
} 
//------------------------------------------------------------------------
int sui_dinfo_dbuff_rd_dbuff(sui_dinfo_t *di, long idx, ul_dbuff_t *dbuf) 
{
  if(!di->rdval) return SUI_RET_EPERM;
  if(di->tinfo != SUI_TYPE_DBUFF) return SUI_RET_EPERM;
  return di->rdval(di, idx, dbuf);
  //    mylog(LOG_INF, "Reading unknown data to dbuff of length 8.\n");
}

//------------------------------------------------------------------------

int sui_dinfo_dbuff_wr_dbuff( sui_dinfo_t *di, long idx, const ul_dbuff_t *dbuf) 
{
  if(!di->wrval) return SUI_RET_EPERM;
  if(di->tinfo != SUI_TYPE_DBUFF) return SUI_RET_EPERM;
  return di->wrval(di, idx, dbuf);
  //    mylog(LOG_INF, "Skipping writing dbuff to unknown data structure.\n");
}
//------------------------------------------------------------------------

int sui_dinfo_dbuff_rd_long(sui_dinfo_t *di, long idx, long *buf)
{
    int l, s = sizeof(*buf);
    int ret;
    ul_dbuff_t db;
    ul_dbuff_init(&db, UL_DBUFF_IS_STATIC);
    if(!di->rdval) return SUI_RET_EPERM;
    if(di->tinfo != SUI_TYPE_DBUFF) return SUI_RET_EPERM;
    ret = di->rdval(di, idx, &db);
    if(ret != SUI_RET_OK) return ret;
    l = db.len;
    l = (l > s)? s: l;
    memset(buf, 0, s);
    memcpy(buf, db.data, l);
    return SUI_RET_OK;
}
//------------------------------------------------------------------------

int sui_dinfo_dbuff_wr_long(sui_dinfo_t *di, long idx, const long *buf)
{
    int s = sizeof(*buf);
    int ret;
    ul_dbuff_t db;
    ul_dbuff_init(&db, UL_DBUFF_IS_STATIC);
    if(!di->wrval) return SUI_RET_EPERM;
    if(di->tinfo != SUI_TYPE_DBUFF) return SUI_RET_EPERM;
    ul_dbuff_set_len(&db, s);
    if(db.len != s) return SUI_RET_EPERM;
    memcpy(db.data, buf, s);
    ret = di->wrval(di, idx, &db);
    return SUI_RET_OK;
}
//==================================================================

sui_dinfo_t *sui_dinfo_dbuff_create(ul_dbuff_t *db, long aidxsize) 
{  
    sui_datai_rdfnc_t *ard = sui_dbuff_rdval; 
    sui_datai_wrfnc_t *awr = sui_dbuff_wrval;
    sui_dinfo_t *dinfo;
    
    dinfo = sui_create_dinfo(db, 0, 0, 0, 0, ard, awr);
    if(dinfo) {
        dinfo->idxsize = aidxsize;
        dinfo->tinfo = SUI_TYPE_DBUFF;
    }
    return dinfo;
}

