#ifndef _SUI_DINFO_DBUFF_H_
#define _SUI_DINFO_DBUFF_H_

#include "ul_dbuff.h"
#include "suiut/sui_dinfo.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * sui_create_dinfo_dbuff - Creates DINFO for &ul_dbuff structure
 * @db:       Pointer to the dbuff
 * @aidxsize: Allowed range of indexes form 0 to @aidxsize-1, if zero then no check
 *
 * Returns: Pointer to newly created DINFO.
 * File: sui_dinfo_dbuff.c
 */
sui_dinfo_t *sui_dinfo_dbuff_create(ul_dbuff_t *db, long aidxsize);

/**
 * sui_dinfo_dbuff_rd_dbuff - Reads &ul_dbuff data from specified DINFO
 * @di:    Pointer to the DIONFO
 * @idx:   Index of read data inside DINFO.
 * @dbuf:  Pointer to where the read value is stored 
 *
 * Return Value: Operation result code, %SUI_RET_OK in the case of success.
 * File: sui_dinfo_dbuff.c
 */
int sui_dinfo_dbuff_rd_dbuff(sui_dinfo_t *di, long idx, ul_dbuff_t *dbuf); 

/**
 * sui_dinfo_dbuff_wr_dbuff - Writes &ul_dbuff data to specifies DINFO
 * @di:    Pointer to the DIONFO
 * @idx:   Index of read data inside DINFO.
 * @dbuf:  Pointer to the dbuff
 *
 * Return Value: Operation result code, %SUI_RET_OK in the case of success.
 * File: sui_dinfo_dbuff.c
 */
int sui_dinfo_dbuff_wr_dbuff(sui_dinfo_t *di, long idx, const ul_dbuff_t *dbuf);

/**
 * sui_dinfo_dbuff_rd_long - Reads long integer data from specified dbuff DINFO
 * @di:    Pointer to the DIONFO
 * @idx:   Index of read data inside DINFO.
 * @buf:  Pointer to the dbuff
 *
 * Return Value: Operation result code, %SUI_RET_OK in the case of success.
 * File: sui_dinfo_dbuff.c
 */
int sui_dinfo_dbuff_rd_long(sui_dinfo_t *di, long idx, long *buf);

/**
 * sui_dinfo_dbuff_wr_long - Writes long integer data to specified dbuff DINFO
 * @di:    Pointer to the DIONFO
 * @idx:   Index of read data inside DINFO.
 * @buf:  Pointer to the dbuff
 *
 * Return Value: Operation result code, %SUI_RET_OK in the case of success.
 * File: sui_dinfo_dbuff.c
 */
int sui_dinfo_dbuff_wr_long(sui_dinfo_t *di, long idx, const long *buf);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUI_DINFO_DBUFF_H_ */
