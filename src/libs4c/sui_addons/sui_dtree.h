#ifndef _SUI_DTREE_H
#define _SUI_DTREE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <suiut/sui_dinfo.h>

#define SUI_DTREE_FOUND   1
#define SUI_DTREE_DIR     2
#define SUI_DTREE_NOPATH -1
#define SUI_DTREE_ERROR  -2

struct sui_dtree_dir_t;

typedef int sui_dtree_lookup_partial_t(struct sui_dtree_dir_t *from_dir, const char *path, int *consumed,
                     struct sui_dtree_dir_t **found_dir, sui_dinfo_t **datai);

typedef struct sui_dtree_dir_t {
    sui_dtree_lookup_partial_t *lookup;
} sui_dtree_dir_t;


int sui_dtree_lookup(sui_dtree_dir_t *from_dir, const char *path, sui_dtree_dir_t **found_dir,
                     sui_dinfo_t **datai);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUI_DTREE_H */
