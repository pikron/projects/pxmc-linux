#ifndef CMD_PROC_PRIV_H
#define CMD_PROC_PRIV_H

/* The maximal depth of command arrays nesting (see CMD_DES_INCLUDE_SUBLIST). */
#define CMD_ARR_STACK_SIZE 4    

char *skip_white(char *p);
int cmd_io_line_out(cmd_io_t *cmd_io);
int cmd_io_line_in(cmd_io_t *cmd_io);

#include <stdio.h>
#define DPRINT(...) fprintf(stderr, __VA_ARGS__)

#endif
