#ifndef CMD_BTH_H
#define CMD_BTH_H

/* Bluetooth specific */

cmd_des_t const **cmd_bth;

extern cmd_io_t cmd_io_bth;

int cmd_bth_line_out(cmd_io_t *cmd_io);

int cmd_bth_line_in(cmd_io_t *cmd_io);

char *cmd_bth_rdline(cmd_io_t *cmd_io, int mode);

int cmd_bth_processor_run(void);

#endif
