/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware  
 
  cmdio_std_file.c - interconnection of text command processor
                  with generic file interface
 
  Copyright (C) 2001-2009 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2002-2009 by PiKRON Ltd. http://www.pikron.com
            (C) 2007 by Michal Sojka <sojkam1@fel.cvut.cz>

  This file can be used and copied according to next
  license alternatives
   - MPL - Mozilla Public License
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <ctype.h>
#include <malloc.h>
#include <string.h>
#include <cmd_proc.h>
#include <stdio.h>

cmd_des_t const **cmd_forshell;

static int cmd_io_putc_forfile(struct cmd_io *cmd_io,int ch)
{
  int res=fputc(ch,cmd_io->priv.file.out);
  res=(res==EOF)?-1:0;
  return res;
}

static int cmd_io_getc_forfile(struct cmd_io *cmd_io)
{
  int ch=fgetc(cmd_io->priv.file.in);
  if(ch==EOF) ch=-1;
  return ch;
}

static  int cmd_io_write_forfile(struct cmd_io *cmd_io,const void *buf,int count)
{
  int res=fwrite(buf, 1, count, cmd_io->priv.file.out);
  return res;
}

static  int cmd_io_read_forfile(struct cmd_io *cmd_io,void *buf,int count)
{
  int res=fread(buf, 1, count, cmd_io->priv.file.in);
  return res;
}

int cmd_proc_forfile(FILE * in,FILE * out, cmd_des_t const **des_arr, char *line)
{
  cmd_io_t cmd_io;
  
  cmd_io.putc=cmd_io_putc_forfile;
  cmd_io.getc=cmd_io_getc_forfile;
  cmd_io.write=cmd_io_write_forfile;
  cmd_io.read=cmd_io_read_forfile;
  cmd_io.priv.file.in=in;
  cmd_io.priv.file.out=out;

  return proc_cmd_line(&cmd_io, des_arr, line);
}

int cmd_proc_forshell (int argc, char **argv)
{
  int i;
  int res;
  size_t len;
  char *line, *p;
  
  if(argc<2){
    cmd_proc_forfile(stdin, stdout, cmd_forshell, "help");
    printf("\n");
    return 1;
  }
  
  for(len=0, i=1; i<argc; i++)
    len+=strlen(argv[i])+1;

  line=malloc(len);
  if(!line)
    return 3;
  
  for(p=line, i=1; i<argc; i++) {
    len=strlen(argv[i]);
    memcpy(p,argv[i],len);
    p+=len;
    *(p++)=' ';
  }
  *(--p)=0;

  res=cmd_proc_forfile(stdin, stdout, cmd_forshell, line);
  printf("\n");

  if(res<0) {
    printf("cmdproc \"%s\" failed with error %d\n",line,res);
    res=2;
  } else res=0;
  
  free(line);
  
  return res;

}
