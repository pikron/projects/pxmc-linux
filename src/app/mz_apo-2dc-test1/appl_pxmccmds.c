/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmccmds.c - position controller RoCoN specific commands

  Copyright (C) 2001-2013 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2013 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <system_def.h>
#include <pxmc.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>

#include <utils.h>

#include "pxmc_cmds.h"

#include "appl_defs.h"
#include "appl_pxmc.h"

/**
 * cmd_do_axis_mode - checks the command format and busy flag validity, calls pxmc_axis_mode
 *
 * if pxmc_axis_mode returns -1, cmd_do_axis_mode returns -1.
 */
int cmd_do_axis_mode(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  pxmc_state_t *mcs;

  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  if(*param[2]=='?') {
    return cmd_opchar_replong(cmd_io, param, pxmc_axis_rdmode(mcs), 0, 0);
  }

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(mcs->pxms_flg&PXMS_BSY_m) return -CMDERR_BSYREG;

  val=atol(param[3]);
  val=pxmc_axis_mode(mcs,val);
  if(val<0)
    return val;

  return 0;
}

cmd_des_t const cmd_des_axis_mode={0, CDESM_OPCHR|CDESM_WR,
                        "REGMODE?","axis working mode",cmd_do_axis_mode,
                         {}};

cmd_des_t const *cmd_appl_pxmc[] =
{
  &cmd_des_axis_mode,
  NULL
};
