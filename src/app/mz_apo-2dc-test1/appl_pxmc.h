/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  appl_pxmc.h - SPI connected motor control board specific
                extensions

  (C) 2001-2021 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2021 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _APPL_PXMC_H_
#define _APPL_PXMC_H_

#include <stdint.h>
#include <pxmc.h>

struct z2dcdrv1_state_t;

typedef struct pxmc_z2dcdrv1_state_t {
  pxmc_state_t base;
  struct z2dcdrv1_state_t *z2dcdrv1st;
} pxmc_z2dcdrv1_state_t;

extern int appl_errstop_mode;
extern int appl_idlerel_time;
int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits);

int pxmc_z2dcdrv1_pwm_direct_wr(unsigned chan, unsigned pwm, int en);

#endif /*_APPL_PXMC_H_*/
