/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmc.c - SPI connected motor control board specific
                extensionsposition

  Copyright (C) 2001-2021 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2021 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include <pxmc_gen_info.h>
#include <pxmc_dq_trans.h>
#include <pxmc_sin_fixed.h>
#include <stdlib.h>
#include <string.h>

#if !defined(clzl) && !defined(HAVE_CLZL)
#define clzl __builtin_clzl
#endif

#include "appl_defs.h"
#include "appl_pxmc.h"
#include "appl_utils.h"
#include "zynq_2dcdrv1_mc.h"
#include "mzapo_regs.h"

pthread_t pxmc_base_thread_id;

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                           unsigned long index_irc, int diff2err);

#define PXML_MAIN_CNT 2

unsigned pxmc_z2dcdrv1_pwm_magnitude = PXMC_Z2DCDRV1_PWM_CYCLE;

int appl_errstop_mode = 0;

static inline
pxmc_z2dcdrv1_state_t *pxmc_state2z2dcdrv1_state(pxmc_state_t *mcs)
{
  pxmc_z2dcdrv1_state_t *mcsrc;
 #ifdef UL_CONTAINEROF
  mcsrc = UL_CONTAINEROF(mcs, pxmc_z2dcdrv1_state_t, base);
 #else /*UL_CONTAINEROF*/
  mcsrc = (pxmc_z2dcdrv1_state_t*)((char*)mcs - __builtin_offsetof(pxmc_z2dcdrv1_state_t, base));
 #endif /*UL_CONTAINEROF*/
  return mcsrc;
}

int
pxmc_inp_z2dcdrv1_inp(struct pxmc_state *mcs)
{
  pxmc_z2dcdrv1_state_t *mcsrc = pxmc_state2z2dcdrv1_state(mcs);
  /*int chan=mcs->pxms_inp_info;*/
  uint32_t irc_val;
  long irc;
  long pos;

  if (z2dcdrv1_irc_read(mcsrc->z2dcdrv1st, &irc_val) < 0)
     return -1;;

  irc_val += mcsrc->z2dcdrv1st->pos_offset;
  irc = irc_val;

  pos = irc << PXMC_SUBDIV(mcs);
  mcs->pxms_as = pos - mcs->pxms_ap;
  mcs->pxms_ap = pos;

  /* Running of the motor commutator */
  if (mcs->pxms_flg & PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs, irc);

  return 0;
}

int
pxmc_inp_z2dcdrv1_ap2hw(struct pxmc_state *mcs)
{
  pxmc_z2dcdrv1_state_t *mcsrc = pxmc_state2z2dcdrv1_state(mcs);
  /*int chan=mcs->pxms_inp_info;*/
  uint32_t irc_val;
  long pos_diff;

  if (z2dcdrv1_irc_read(mcsrc->z2dcdrv1st, &irc_val) < 0)
     return -1;

  pos_diff = mcs->pxms_ap - (irc_val << PXMC_SUBDIV(mcs));

  irc_val = pos_diff >> PXMC_SUBDIV(mcs);

  /* Adjust phase table alignemt to modified IRC readout  */
  mcs->pxms_ptofs += irc_val - mcsrc->z2dcdrv1st->pos_offset;

  mcsrc->z2dcdrv1st->pos_offset = irc_val;
  return 0;
}

/**
 * pxmc_z2dcdrv1_pwm_dc_out - DC motor CW and CCW PWM output
 * @mcs:  Motion controller state information
 */
int
pxmc_z2dcdrv1_pwm_dc_out(pxmc_state_t *mcs)
{
  pxmc_z2dcdrv1_state_t *mcsrc = pxmc_state2z2dcdrv1_state(mcs);
  /* int chan = mcs->pxms_out_info; */
  int ene = mcs->pxms_ene;

  z2dcdrv1_bidirpwm_set(mcsrc->z2dcdrv1st, ene);

  return 0;
}

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                           unsigned long index_irc, int diff2err)
{
  return -1;
}

/**
 * pxmc_dummy_con - Dummy controller for synchronous BLDC/PMSM/steper drive
 * @mcs:        Motion controller state information
 */
int
pxmc_dummy_con(pxmc_state_t *mcs)
{
  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return NULL;
}

z2dcdrv1_state_t z2dcdrv1_state0 = {
  .regs_base_phys = DCSPDRV_REG_BASE_PHYS_0,
};

z2dcdrv1_state_t z2dcdrv1_state1 = {
  .regs_base_phys = DCSPDRV_REG_BASE_PHYS_1,
};

pxmc_z2dcdrv1_state_t mcs0 =
{
.base = {
.pxms_flg =
  PXMS_ENI_m,
.pxms_do_inp =
  pxmc_inp_z2dcdrv1_inp,
.pxms_do_con =
  pxmc_pid_con /*pxmc_dummy_con*/,
.pxms_do_out =
  pxmc_z2dcdrv1_pwm_dc_out,
  .pxms_do_deb = 0,
  .pxms_do_gen = 0,
.pxms_do_ap2hw =
  pxmc_inp_z2dcdrv1_ap2hw,
  .pxms_ap = 0, .pxms_as = 0,
  .pxms_rp = 55 * 256, .pxms_rs = 0,
 #ifndef PXMC_WITH_FIXED_SUBDIV
  .pxms_subdiv = 8,
 #endif /*PXMC_WITH_FIXED_SUBDIV*/
  .pxms_md = 800 << 8, .pxms_ms = 500, .pxms_ma = 10,
  .pxms_inp_info = 0,
  .pxms_out_info = 0,
  .pxms_ene = 0, .pxms_erc = 0,
  .pxms_p = 40, .pxms_i = 10, .pxms_d = 100, .pxms_s1 = 0, .pxms_s2 = 0,
  .pxms_me = 0x7e00/*0x7fff*/,
.pxms_cfg =
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
  PXMS_CFG_I2PT_m * 0 | 0x2,

  .pxms_ptper = 1,
  .pxms_ptirc = 1000,
  .pxms_ptmark = 1180,
  /*.pxms_ptamp = 0x7fff,*/

  .pxms_hal = 0x40,
},
  .z2dcdrv1st = &z2dcdrv1_state0,
};

pxmc_z2dcdrv1_state_t mcs1 =
{
.base = {
.pxms_flg =
  PXMS_ENI_m,
.pxms_do_inp =
  pxmc_inp_z2dcdrv1_inp,
.pxms_do_con =
  pxmc_pid_con /*pxmc_dummy_con*/,
.pxms_do_out =
  pxmc_z2dcdrv1_pwm_dc_out,
  .pxms_do_deb = 0,
  .pxms_do_gen = 0,
.pxms_do_ap2hw =
  pxmc_inp_z2dcdrv1_ap2hw,
  .pxms_ap = 0, .pxms_as = 0,
  .pxms_rp = 55 * 256, .pxms_rs = 0,
 #ifndef PXMC_WITH_FIXED_SUBDIV
  .pxms_subdiv = 8,
 #endif /*PXMC_WITH_FIXED_SUBDIV*/
  .pxms_md = 800 << 8, .pxms_ms = 500, .pxms_ma = 10,
  .pxms_inp_info = 0,
  .pxms_out_info = 0,
  .pxms_ene = 0, .pxms_erc = 0,
  .pxms_p = 40, .pxms_i = 10, .pxms_d = 100, .pxms_s1 = 0, .pxms_s2 = 0,
  .pxms_me = 0x7e00/*0x7fff*/,
.pxms_cfg =
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
  PXMS_CFG_I2PT_m * 0 | 0x2,

  .pxms_ptper = 1,
  .pxms_ptirc = 1000,
  .pxms_ptmark = 1180,
  /*.pxms_ptamp = 0x7fff,*/

  .pxms_hal = 0x40,
},
  .z2dcdrv1st = &z2dcdrv1_state1,
};

pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT] = {&mcs0.base, &mcs1.base};

pxmc_state_list_t  pxmc_main_list =
{
  .pxml_arr = pxmc_main_arr,
  .pxml_cnt = 0
};


static inline void pxmc_sfi_input(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENI_m - check if input (IRC) update is enabled */
    if (mcs->pxms_flg & PXMS_ENI_m)
    {
      pxmc_call(mcs, mcs->pxms_do_inp);
    }
  }
}

static inline void pxmc_sfi_controller_and_output(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENR_m - check if controller is enabled */
    if (mcs->pxms_flg & PXMS_ENR_m || mcs->pxms_flg & PXMS_ENO_m)
    {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg & PXMS_ENR_m)
      {

        pxmc_call(mcs, mcs->pxms_do_con);

        /* PXMS_ERR_m - if axis in error state */
        if (mcs->pxms_flg & PXMS_ERR_m)
          mcs->pxms_ene = 0;
      }

      /* for bushless motors, it is necessary to call do_out
        even if the controller is not enabled and PWM should be provided. */
      pxmc_call(mcs, mcs->pxms_do_out);
    }
  }
}

static inline void pxmc_sfi_generator(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENG_m - check if requested value (position) generator is enabled */
    if (mcs->pxms_flg & PXMS_ENG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_gen);
    }
  }
}

static inline void pxmc_sfi_dbg(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    if (mcs->pxms_flg & PXMS_DBG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_deb);
    }
  }
}


void pxmc_samplig_period(void)
{
  pxmc_sfi_input();
  pxmc_sfi_controller_and_output();
  pxmc_sfi_generator();
  pxmc_sfi_dbg();

  do_pxmc_coordmv();
}

void *pxmc_base_thread(void *arg)
{
  sample_period_t sample_period;
  sample_period_setup(&sample_period, 1000 * 1000);

  do {
    pxmc_samplig_period();
    sample_period_wait_next(&sample_period);
  } while(1);
}

int pxmc_clear_power_stop(void)
{
  return 0;
}

int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits)
{
  unsigned short flg;
  unsigned short chan;
  unsigned long busy_bits = 0;
  unsigned long error_bits = 0;
  pxmc_state_t *mcs;
  flg=0;
  pxmc_for_each_mcs(chan, mcs) {
    if(mcs) {
      flg |= mcs->pxms_flg;
      if (mcs->pxms_flg & PXMS_BSY_m)
        busy_bits |= 1 << chan;
      if (mcs->pxms_flg & PXMS_ERR_m)
        error_bits |= 1 << chan;
    }
  }
  if (appl_errstop_mode) {
    if((flg & PXMS_ENG_m) && (flg & PXMS_ERR_m)) {
      pxmc_for_each_mcs(chan, mcs) {
        if(mcs&&(mcs->pxms_flg & PXMS_ENG_m)) {
          pxmc_stop(mcs, 0);
        }
      }
    }
  }

  if (pbusy_bits != NULL)
    *pbusy_bits = busy_bits;
  if (perror_bits != NULL)
    *perror_bits = error_bits;

  return flg;
}

int pxmc_axis_rdmode(pxmc_state_t *mcs)
{
  if (mcs->pxms_do_out == pxmc_z2dcdrv1_pwm_dc_out)
    return PXMC_AXIS_MODE_DC;
  return -1;
}

/**
 * pxmc_axis_mode - Sets axis mode.[extern API]
 * @mcs:        Motion controller state information
 * @mode:       0 .. previous mode, 1 .. stepper motor mode,
 *              2 .. stepper motor with IRC feedback and PWM ,
 *              3 .. stepper motor with PWM control
 *              4 .. DC motor with IRC feedback and PWM
 *
 */
int
pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
  int res = 0;
  int prev_mode;

  pxmc_axis_release(mcs);
  pxmc_clear_flag(mcs, PXMS_ENI_b);
  pxmc_clear_flag(mcs,PXMS_ENO_b);
  /*TODO Clear possible stall index flags from hardware */

  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);

  prev_mode = pxmc_axis_rdmode(mcs);

  if (mode == PXMC_AXIS_MODE_NOCHANGE)
    mode = prev_mode;
  if (mode < 0)
    return -1;
  if (!mode)
    mode = PXMC_AXIS_MODE_DC;

  switch (mode) {
    case PXMC_AXIS_MODE_DC:
      mcs->pxms_do_out = pxmc_z2dcdrv1_pwm_dc_out;
      break;
    default:
      return -1;
  }

  /*TODO Clear possible stall index flags from hardware */

  /* Request new phases alignment for changed parameters */
  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);
  pxmc_set_flag(mcs, PXMS_ENI_b);
  return res;
}

int pxmc_done(void)
{
  int var;
  pxmc_state_t *mcs;

  if (!pxmc_main_list.pxml_cnt)
    return 0;

  pxmc_for_each_mcs(var, mcs)
  {
    pxmc_axis_release(mcs);
  }

  pxmc_main_list.pxml_cnt = 0;
  __memory_barrier();

  return 0;
}

int pxmc_initialize(void)
{
  pxmc_main_list.pxml_cnt = 0;
  pxmc_dbg_hist = NULL;

  if (z2dcdrv1_init(mcs0.z2dcdrv1st) < 0)
    return -1;

  if (z2dcdrv1_init(mcs1.z2dcdrv1st) < 0)
    return -1;

  __memory_barrier();
  pxmc_main_list.pxml_cnt = PXML_MAIN_CNT;

  if (create_rt_task(&pxmc_base_thread_id, 60, pxmc_base_thread, NULL) < 0)
    return -1;

  return 0;
}
