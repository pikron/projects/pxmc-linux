#ifndef _ZYNQ_Z2DCDRV1_MC_H
#define _ZYNQ_Z2DCDRV1_MC_H

#include <stdint.h>

#define PXMC_Z2DCDRV1_PWM_CYCLE 5000

typedef struct z2dcdrv1_state_t {
  uintptr_t regs_base_phys;
  void     *regs_base_virt;
  uint32_t  pos_offset; 
} z2dcdrv1_state_t;

int z2dcdrv1_init(z2dcdrv1_state_t *z2dcdrv1st);

int z2dcdrv1_irc_read(z2dcdrv1_state_t *z2dcdrv1st, uint32_t *irc_val);

void z2dcdrv1_bidirpwm_set(z2dcdrv1_state_t *z2dcdrv1st, int32_t pwm_val);

#endif /*_ZYNQ_Z2DCDRV1_MC_H*/
