/*
  Communication with Zynq equipped by 3-phase
  motor driver connected to MicroZed on MZ_APO board.
  MZ_APO and 3-phase motor driver boards have been
  designed by Petr Porazil for PiKRON company.
  The Zynq VHDL design by Pavel Pisa, partially
  inspired by previous work done together with
  Martin Prudek.

  (C) 2017-2021 by Pavel Pisa ppisa@pikron.com
*/

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "zynq_2dcdrv1_mc.h"

char *memdev="/dev/mem";

static inline
uint32_t z2dcdrv1_reg_rd(z2dcdrv1_state_t *z2dcdrv1st, unsigned reg_offs)
{
  return *(volatile uint32_t*)((char*)z2dcdrv1st->regs_base_virt + reg_offs);
}

static inline
void z2dcdrv1_reg_wr(z2dcdrv1_state_t *z2dcdrv1st, unsigned reg_offs, uint32_t val)
{
  *(volatile uint32_t*)((char*)z2dcdrv1st->regs_base_virt + reg_offs) = val;
}

int z2dcdrv1_irc_read(z2dcdrv1_state_t *z2dcdrv1st, uint32_t *irc_val)
{
  *irc_val = z2dcdrv1_reg_rd(z2dcdrv1st, DCSPDRV_REG_IRC_o);
  return 0;
}

void z2dcdrv1_bidirpwm_set(z2dcdrv1_state_t *z2dcdrv1st, int32_t pwm_val)
{
  if (pwm_val > 0)
    z2dcdrv1_reg_wr(z2dcdrv1st, DCSPDRV_REG_DUTY_o, pwm_val | DCSPDRV_REG_DUTY_DIR_A_m);
  else if (pwm_val < 0)
    z2dcdrv1_reg_wr(z2dcdrv1st, DCSPDRV_REG_DUTY_o, -pwm_val | DCSPDRV_REG_DUTY_DIR_B_m);
  else
    z2dcdrv1_reg_wr(z2dcdrv1st, DCSPDRV_REG_DUTY_o, 0);
}

int z2dcdrv1_init(z2dcdrv1_state_t *z2dcdrv1st)
{
  int ret = 0;
  uint32_t irc_val;

  if (z2dcdrv1st->regs_base_phys == 0) {
    ret = -1;
    return ret;
  }

  z2dcdrv1st->regs_base_virt = map_phys_address(z2dcdrv1st->regs_base_phys,
                                                DCSPDRV_REG_SIZE, 0);

  if (z2dcdrv1st->regs_base_virt == NULL) {
    ret = -1;
    return ret;
  }

  z2dcdrv1_reg_wr(z2dcdrv1st, DCSPDRV_REG_DUTY_o, 0);
  z2dcdrv1_reg_wr(z2dcdrv1st, DCSPDRV_REG_PERIOD_o, PXMC_Z2DCDRV1_PWM_CYCLE);
  z2dcdrv1_reg_wr(z2dcdrv1st, DCSPDRV_REG_CR_o, DCSPDRV_REG_CR_PWM_ENABLE_m);

  z2dcdrv1_irc_read(z2dcdrv1st, &irc_val);
  z2dcdrv1st->pos_offset = -irc_val;

  return ret;
}
