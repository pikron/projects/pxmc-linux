/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  math_sqrtll.c - fast routine for 64-bit unsigned square root

  Copyright (C) 2001-2014 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2014 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#define _GNU_SOURCE

#include <string.h>
#include <stdint.h>

/*
	y0 := k - T1[31&(k>>15)].	... y ~ sqrt(x) to 8 bits

	y := (y+x/y)/2		... almost 17 sig. bits
	y := (y+x/y)/2		... almost 35 sig. bits
*/

#ifdef __GNUC__
uint32_t __attribute__((unused))
#else
uint32_t
#endif
  sqrtll_T1[64]={
    2147418619,2146907255,2145906284,2144436844,
    2142518597,2140169865,2137407758,2134248281,
    2130706432,2126796288,2122531084,2117923281,
    2112984627,2107726213,2102158525,2096291488,
    2090134507,2083696503,2076985953,2070010911,
    2062779045,2055297659,2047573718,2039613867,
    2031424454,2023011548,2014380954,2005538230,
    1996488704,1987237480,1977789459,1968149347,
    1970116061,1983291585,1995774699,2007595298,
    2018781187,2029358280,2039350771,2048781297,
    2057671066,2066039988,2073906780,2081289063,
    2088203452,2094665633,2100690434,2106291892,
    2111483306,2116277295,2120685844,2124720346,
    2128391644,2131710068,2134685466,2137327238,
    2139644360,2141645415,2143338612,2144731814,
    2145832551,2146648046,2147185228,2147450751
};

#if 1

unsigned long sqrtll(unsigned long long x)
{
  uint32_t y,t1,t2;
  uint32_t x0 = x, x1 = x >> 32;

 #ifndef __GNUC__

  if (x1) {			/* x[32:63] != 0 */
    if (x1 & 0xffff0000){	/* x[48:63] != 0 */
      t1 = x1;
      if (x1 >= 0xfffffffe)	/* exception bypass */
        return 0xffffffff;
      t2 = 63;
    } else {			/* x[48:63] == 0 */
      t1 = x0 >> 16 | x1 << 16;
      t2 = 47;
    }
  } else {			/* x[32:64] == 0 */
    if (!x0) return 0;
    if (x0 & 0xffff0000){	/* x[16:31] != 0 */
      t1 = x0;
      t2 = 31;
    } else {			/* x[16:31] == 0 */
      t1 = x0 << 16;
      t2 = 15;
    }
  }

  while (!(t1 & 0x80000000)) {
    t1 <<= 1;
    t2--;
  }
 #else /*__GNUC__*/
  if (x1) {			/* x[32:63] != 0 */
    if (x1 >= 0xfffffffe)	/* exception bypass */
      return 0xffffffff;
    t2 = __builtin_clz(x1);
    t1 = (x1 << t2) | ((x0 >> (32 - t2)));
    t2 = 63 - t2;
  } else {
    if (!x0)
      return 0;
    t2 = __builtin_clz(x0);
    t1 = x0 << t2;
    t2 = 31 - t2;
  }
 #endif /*__GNUC__*/

  if (t2 & 1){
    t1 |= 0x80000000;
  } else {
    t1 &= ~0x80000000;
  }
  t2 >>= 1;

  y = t1;
  t1 >>= 32 - 6;
  y >>= 1;
  y += sqrtll_T1[t1];
  if (t2 != 31) {
    y >>= 31 - t2 - 1;
    y = (y >> 1) + (y & 1);
  }
 #if 1
  if (!x1) {
    y= (y + x0 / y + 1) / 2;
    y= (y + x0 / y + 1) / 2;
  } else if (x1 < 0x00010000) {
    uint32_t y0, y1, y2, xt, r;

    xt = (x1 << 16) | (x0 >> 16);
    y2 = xt / y;
    r = xt % y;
    xt = (r << 8);
    y1 = xt / y;
    r = xt % y;
    xt = (r << 8) + (x0 & 0xffff);
    y0 = (xt / y) + (y1 << 8) + (y2 << 16);
    y = (y + y0 + 1) / 2;

    xt = (x1 << 16) | (x0 >> 16);
    y2 = xt / y;
    r = xt % y;
    xt = (r << 8);
    y1 = xt / y;
    r = xt % y;
    xt = (r << 8) + (x0 & 0xffff);
    y0 = (xt / y) + (y1 << 8) + (y2 << 16);
    y = (y + y0 + 1) / 2;

  } else
 #endif
  {
    y = (y + x / y + 1) / 2;
    y = (y + x / y + 1) / 2;
  }
  return y;
}

#ifdef __GNUC__

unsigned long sqrtll_approx(unsigned long long x)
{
  unsigned int r;
  uint32_t x1 = x >> 32;
  if (x1 < 0x00010000)
    return sqrtll(x);

  r = __builtin_clz(x1);
  r = (17 - r) / 2;
  x1 = sqrtll(x >> (r * 2));

  return x1 << r;
}

#endif /*__GNUC__*/

#else

unsigned long sqrtll(unsigned long long x)
{
  unsigned long y,t1,t2,t3;
  unsigned long x0=x, x1=x>>32;
  __asm__ (
    "	movel	%2,%3\n"
    "	beqs	2f\n"
    "	swap	%3\n"
    "	tstw	%3\n"
    "	beqs	1f\n"
    "	movel	%2,%3\n"	/* x[48:64] != 0 */
    "	movql	#63,%4\n"
    "	cmpl	#0xfffffffe,%3\n"
    "	bcss	4f\n"
    "	movl	#0xffffffff,%0\n"
    "	bras	9f\n"

    "1:	movel	%1,%4\n"	/* x[48:64] == 0 */
    "	swap	%4\n"
    "	orw	%4,%3\n"
    "	movql	#47,%4\n"
    "	bras	4f\n"

    "2:	movel	%1,%3\n"	/* x[32:64] == 0 */
    "	bnes	3f\n"
    "	clrl	%0\n"		/* x==0 */
    "	bras	9f\n"
    "3:	swap	%3\n"
    "	movql	#15,%4\n"
    "	tstw	%3\n"
    "	beqs	4f\n"
    "	movel	%1,%3\n"	/* x[16:31] != 0 */
    "	movql	#31,%4\n"
    "4:	lsll	#1,%3\n"	/* %3 .. normalized */
    "	dbcs	%4,4b\n"	/* %4 .. exponent bit order */
    "	lsrl	#1,%4\n"
    "	roxrl	#1,%3\n"	/* exp%2 -> %4.31, exp/=2 */
    "	movel	%3,%0\n"
    "	roll	#6,%3\n"	/* 6 MSB bits of %4 index to table */
    "	andw	#63,%3\n"
    "	lsrl	#1,%0\n"
    "	addl	@(sqrtll_T1,%3:w:4),%0\n"  /* lin. approximated table */
    "	movql	#31,%3\n"
    "	subl	%4,%3\n"
    "	lsrl	%3,%0\n"	/* denormalize result */

    "	clrl	%5\n"
    "	addxl	%5,%0\n"
    "	movel	%1,%3\n"
    "	movel	%2,%4\n"
    "	divul	%0,%4,%3\n"	/* y := (y+x/y)/2 */
    "   bvcs    5f\n"
    "   subl	%0,%4\n"	/* divission overflow */
    "	divul	%0,%4,%3\n"
    "   addl	%3,%0\n"
    "   movql	#-1,%3\n"
    "5:	addl	%3,%0\n"	/* regular path without ov */
    "	roxrl	#1,%0\n"
    "	addxl	%5,%0\n"
    "6:	movel	%1,%3\n"
    "	movel	%2,%4\n"
    "	divul	%0,%4,%3\n"	/* y := (y+x/y)/2 */
    "	addl	%3,%0\n"
    "	roxrl	#1,%0\n"
    "	addxl	%5,%0\n"
    "9:\n"

    :"=d"(y),"=d"(x0),"=d"(x1),"=d"(t1),"=d"(t2),"=d"(t3)
    :"1"(x0),"2"(x1)
    :"cc"
  );

  return y;
}

#endif

