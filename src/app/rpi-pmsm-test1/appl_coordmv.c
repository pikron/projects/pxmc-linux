/*******************************************************************
  Motion and Robotic System (MARS) aplication components

  appl_coordmv.c - application specific coordinated
                   motions control support

  Copyright (C) 2001-2014 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2014 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include <cmd_proc.h>
#include <pxmc_cmds.h>
#include <utils.h>

#include "appl_defs.h"

int pxmc_coordmv_process(void)
{
  /* coordinator bottom handling */
  if (pxmc_coordmv_state.mcs_flg & MCS_BOTT_m){
    pxmc_coordmv_bottom(&pxmc_coordmv_state);
  }

  return 0;
}

int cmd_do_coordmv(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state = &pxmc_coordmv_state;
  int con_cnt = mcs_state->mcs_con_cnt;
  int i;
  long final[con_cnt], val;
  long mintim = 0;
  char *s;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  s = param[3];
  if ((long)(des->info[1]) & 1) {	/* coordinated movement with time */
    if (si_long(&s, &mintim, 10) <0 ) return -CMDERR_BADPAR;
    if (mintim < 0) return -CMDERR_BADPAR;
    if (si_fndsep(&s,",") <=0 ) return -CMDERR_BADSEP;
  }
  if (!con_cnt) return -CMDERR_BADCFG;
  i = 0;
  while (1) {
    if(si_long(&s, &val, 10) <0 ) return -CMDERR_BADPAR;
    mcs = pxmc_coordmv_indx2mcs(mcs_state, i);
    if(!mcs) return -CMDERR_BADREG;
    final[i] = val << PXMC_SUBDIV(mcs);
    if(++i >= con_cnt) break;
    if(si_fndsep(&s, ",") <= 0) return -CMDERR_BADSEP;
  }
  si_skspace(&s);
  if(*s) return -CMDERR_GARBAG;

  if ((long)(des->info[1]) & 2){
    if (pxmc_coordmv_relmv(mcs_state, con_cnt, final, mintim)<0)
      return -1;
  } else {
    if (pxmc_coordmv_absmv(mcs_state, con_cnt, final, mintim)<0)
      return -1;
  }
  return 0;
}


int cmd_do_coordspline(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state = &pxmc_coordmv_state;
  int con_cnt = mcs_state->mcs_con_cnt;
  long spline_param[con_cnt * PXMC_SPLINE_ORDER_MAX];
  int i;
  int o;
  int a;
  long val;
  long mintim=0;
  long order;
  char *s;
  pxmc_state_t *mcs;
  if (*param[2] != ':') return -CMDERR_OPCHAR;
  s = param[3];
  if ((long)(des->info[1]) & 1) {	/* coordinated movement with time */
    if (si_long(&s,&mintim, 10)<0) return -CMDERR_BADPAR;
    if (mintim<0) return -CMDERR_BADPAR;
    if (si_fndsep(&s, ",")<=0) return -CMDERR_BADSEP;
  }
  if (!con_cnt) return -CMDERR_BADCFG;

  if (si_long(&s,&order, 10) <0 ) return -CMDERR_BADPAR;
  if (mintim <0 ) return -CMDERR_BADPAR;
  if (si_fndsep(&s, ",") <= 0) return -CMDERR_BADSEP;

  if ((order <= 0) || (order > PXMC_SPLINE_ORDER_MAX))
    return -CMDERR_BADPAR;

  i=0; o=0; a=0;
  while (1) {
    if (si_long(&s, &val, 10) < 0) return -CMDERR_BADPAR;
    mcs = pxmc_coordmv_indx2mcs(mcs_state, a);
    if (!mcs) return -CMDERR_BADREG;
    spline_param[i] = val << PXMC_SUBDIV(mcs);;
    i++;
    if (++o >= order) {
      o = 0;
      if (++a >= con_cnt) break;
    }
    if (si_fndsep(&s, ",") <= 0) return -CMDERR_BADSEP;
  }
  si_skspace(&s);
  if(*s) return -CMDERR_GARBAG;

  if (pxmc_coordmv_spline(mcs_state, i, spline_param, order, mintim) < 0)
    return -1;
  return 0;
}


int cmd_do_coordgrp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state = &pxmc_coordmv_state;
  pxmc_state_list_t *reg_list = mcs_state->mcs_con_list;
  int reg_max = reg_list->pxml_cnt;
  int ret;
  unsigned chan;
  int con_indx[reg_max];
  int con_cnt=0;
  char *s;
  if (*param[2] != ':') return -CMDERR_OPCHAR;
  s = param[3];
  do{
    si_skspace(&s);
    if (!*s) break;
    chan = *(s++) - 'A';
    if (chan >= reg_max) return -CMDERR_BADREG;
    con_indx[con_cnt] = chan;
    con_cnt++;
    if ((ret = si_fndsep(&s, ",")) < 0) return -CMDERR_BADSEP;
    if (ret == 0) break;
  } while (1);

  return pxmc_coordmv_grp(mcs_state, con_cnt, con_indx);
}

cmd_des_t const cmd_des_coordmv={0, CDESM_OPCHR|CDESM_WR,
			"COORDMV","initiate coordinated movement to point f1,f2,...",cmd_do_coordmv,
			{0,0}};
cmd_des_t const cmd_des_coordmvt={0, CDESM_OPCHR|CDESM_WR,
			"COORDMVT","coord. movement with time to point mintime,f1,f2,...",cmd_do_coordmv,
			{0,(char*)1}};
cmd_des_t const cmd_des_coordrelmvt={0, CDESM_OPCHR|CDESM_WR,
			"COORDRELMVT","coord. relative movement with time to point mintime,f1,f2,...",
			cmd_do_coordmv,
			{0,(char*)3}};
cmd_des_t const cmd_des_coordsplinet={0, CDESM_OPCHR|CDESM_WR,
			"COORDSPLINET","coord. spline movement with time to point mintime,order,a11,a12,...,a21,..",
			cmd_do_coordspline,
			{0,(char*)1}};
cmd_des_t const cmd_des_coordgrp={0, CDESM_OPCHR|CDESM_WR,
			"COORDGRP","group axes for COORDMV, for ex. C,D,F",cmd_do_coordgrp,
			{0,0}};
cmd_des_t const cmd_des_coorddiscont={0, CDESM_OPCHR|CDESM_RW,
			"COORDDISCONT","max relative discontinuity between segs",cmd_do_rw_long,
			{(char*)&pxmc_coordmv_state.mcs_disca,
			 0}};

cmd_des_t const *const cmd_pxmc_coordmv[]={
  &cmd_des_coordmv,
  &cmd_des_coordmvt,
  &cmd_des_coordrelmvt,
  &cmd_des_coordsplinet,
  &cmd_des_coordgrp,
  &cmd_des_coorddiscont,
  NULL
};
