#include <system_def.h>
#include <stdio.h>
#include <string.h>

#include "appl_defs.h"
#include "appl_utils.h"

#ifdef APPL_WITH_ZYNQ_DRV
#include "zynq_3pmdrv1_mc.h"
#else
#include "rpi_gpio.h"
#include "rpi_gpclk.h"
#endif

void appl_stop(void)
{
  pxmc_done();
  fprintf(stderr, "Application abnormal termination\n");
  sleep(1);
  /* stop clock pin driving FPGA to ensure failase state */
 #ifndef APPL_WITH_ZYNQ_DRV
  rpi_gpio_direction_output(4, 0);
 #endif
}

/***********************************/
int main(int argc, char *argv[])
{
  appl_setup_environment(argv[0]);

#ifdef APPL_RPI_PMSM_SETUP_CLKOUT

  /* initialize 50 Mhz clock output on gpio 4 */
  if (rpi_peripheral_registers_map() < 0) {
    fprintf(stderr, "%s: rpi_peripheral_registers_map failed\n", argv[0]);
    return -1;
  }

  if (rpi_gpclk_setup(0, RPI_GPCLK_PLLD_500_MHZ, 10, 0) < 0) {
    fprintf(stderr, "%s: rpi_gpclk_setup failed\n", argv[0]);
    return -1;
  }

  if (rpi_gpio_alt_fnc(4 /*gpio*/, 0/*alt_fnc*/) < 0) {
    fprintf(stderr, "%s: rpi_gpio_alt_fnc failed\n", argv[0]);
    return -1;
  }

#endif /* APPL_RPI_PMSM_SETUP_CLKOUT */

  pxmc_initialize();

  do {
    cmdproc_poll();
  } while(1);

  return 0;
}

