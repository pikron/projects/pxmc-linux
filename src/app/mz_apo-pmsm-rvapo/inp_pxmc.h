int pxmc_inp_z3pmdrv1_wmcc_inp(struct pxmc_state *mcs);
void pxmcc_axis_get_cur_dq_filt_raw(pxmc_state_t *mcs,int32_t *p_cur_d_raw, int32_t *p_cur_q_raw);
void pxmcc_axis_cur_dq_raw2filt(int *p_cur, int32_t cur_raw);
#ifdef PXMC_WITH_EXTENDED_STATE
int pxmc_pxmcc_cur_acquire_and_irc_inp(pxmc_state_t *mcs);
#endif /*PXMC_WITH_EXTENDED_STATE*/
