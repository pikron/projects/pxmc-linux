/**
 * Communication with Zynq equipped by 3-phase
 * motor driver connected to MicroZed on MZ_APO board.
 * Specifically, this is the MZ_APO -> motor part.
 * Most of this code is copied from lx-rocon project:
 * https://gitlab.com/pikron/projects/lx_cpu/lx-rocon/-/blob/master/sw/app/rocon/appl_pxmc.c?ref_type=heads
*/
#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
//#include <pxmc_lpc_qei.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include <pxmc_gen_info.h>
#include <stdlib.h>
#include <string.h>

#include "appl_defs.h"
//#include "appl_fpga.h"
#include "appl_pxmc.h"
#include "inp_pxmc.h"
#include "pxmcc_types.h"
#include "zynq_pmsm_rvapo_mc.h"
//#include "pxmcc_interface.h"
#include "inp_pxmc.h"
#include "out_pxmc.h"

#define HAL_ERR_SENSITIVITY 20
#define HAL_ERR_MAX_COUNT    5
/**
 * Conversion of digital hall sensors output to absolute motor position segment.
*/
const unsigned char pxmc_lpc_bdc_hal_pos_table[8] ={
  [0] = 0xff,
  [7] = 0xff,
  [1] = 0, /*0*/
  [5] = 1, /*1*/
  [4] = 2, /*2*/
  [6] = 3, /*3*/
  [2] = 4, /*4*/
  [3] = 5, /*5*/
};

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,unsigned long index_irc, int diff2err){
  long ofsl;
  short ofs;

  ofs = ofsl = index_irc - mcs->pxms_ptmark;//pxms_ptmark is a constant

  if (diff2err){
    short diff;
    diff = (unsigned short)ofs - (unsigned short)mcs->pxms_ptofs;
    if (diff >= mcs->pxms_ptirc / 2)diff -= mcs->pxms_ptirc;
    if (diff <= -mcs->pxms_ptirc / 2)diff += mcs->pxms_ptirc;
    if (diff < 0)diff = -diff;
    if (diff >= mcs->pxms_ptirc / 6)return -1; 
    }
  else{
    long diff;
    diff = (unsigned long)ofsl - irc;
    ofs = ofsl - (diff / mcs->pxms_ptirc) * mcs->pxms_ptirc;
  }

  mcs->pxms_ptofs = ofs;
  return 1;
}

/**
 * Something to do with irc index event mark.
 * Probably means using the register holding irc value at time of IDX.
*/
int pxmc_inp_rocon_ptofs_from_index_poll(struct pxmc_state *mcs, int diff2err){
  //int chan = mcs->pxms_inp_info;
  long irc;
  long index_irc;

 // if (!(*fpga_irc_state[chan] & FPGA_IRC_STATE_INDEX_EVENT_MASK))return 0; nothing to replace this with
    volatile z3pmdrv1_wmcc_state_t*adapter_state= pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
//    volatile pmsm_3pmdrv1_t *hw_axis = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst->pxmcc_ctrl_struct;
  irc = /*(*((uint32_t*)adapter_state->pxmcc_ctrl_struct->inp_info))*/pxmcc_read_irc_injectable(mcs)+adapter_state->pos_offset;//fpga_irc[chan]->count + pxmc_rocon_irc_offset[chan];TODO investigate pxmc_rocon_irc_offset
  index_irc = adapter_state->hw_registers->irc_idx_pos+adapter_state->pos_offset;//fpga_irc[chan]->count_index + pxmc_rocon_irc_offset[chan];

  return pxmc_ptofs_from_index(mcs, irc, index_irc, diff2err);
}

/**
 *Sends desired PWM duty on a virtual perpendicular coil to coprocessor. 
*/
static inline void pxmcc_axis_pwm_dq_out(pxmc_state_t *mcs, int pwm_d, int pwm_q){
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst->pxmcc_ctrl_struct;
  mcc_axis->pwm_dq = (pwm_d << 16) | (pwm_q & 0xffff);
}

static inline void pxmcc_cur_ctrl_pi(int *p_pwm, int32_t *p_cur_err_sum,int cur_err, short cur_ctrl_p, short cur_ctrl_i, int max_pwm){
  int pwm;
  int32_t cur_err_sum = *p_cur_err_sum;

  pwm = (cur_err * cur_ctrl_p) >> 8;

  if (cur_ctrl_i)
    cur_err_sum += cur_err * cur_ctrl_i;
  else
    cur_err_sum = 0;

  pwm += cur_err_sum >> 16;

  if (pwm > max_pwm) {
    cur_err_sum -= (pwm - max_pwm) << 16;
    pwm = max_pwm;
  } else if (-pwm > max_pwm) {
    cur_err_sum -= (pwm + max_pwm) << 16;
    pwm = 0 - max_pwm;
  }
  *p_cur_err_sum = cur_err_sum;
  *p_pwm = pwm;
}
/**
 * Updates phase table offset in the coprocessor
 * @enable_update:      Pretty sure that 1 *disables* update.
*/
void pxmcc_pxmc_ptofs2mcc(pxmc_state_t *mcs, int enable_update){
   volatile z3pmdrv1_wmcc_state_t*adapter_state= pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
  volatile pxmcc_axis_data_t *mcc_axis =adapter_state->pxmcc_ctrl_struct;
  //int inp_chan=mcs->pxms_inp_info;
  uint32_t ptofs;
  uint32_t irc;

  if (mcc_axis != NULL) {
    if (enable_update >= 0)mcc_axis->ptirc = 0xffffffff;//this special value disables irc updates in coprocessor
    ptofs = mcs->pxms_ptofs - adapter_state->pos_offset; //pxmc_rocon_irc_offset[inp_chan];
    irc = pxmcc_read_irc_injectable(mcs);//*((uint32_t*)adapter_state->pxmcc_ctrl_struct->inp_info);//fpga_irc[inp_chan]->count;
    ptofs = (int16_t)(ptofs - irc) + irc;
    mcc_axis->ptofs = ptofs;
    if (enable_update > 0)mcc_axis->ptirc = mcs->pxms_ptirc;
    }
}

/**
 * Invalid value received from the digital hall sensors.
 * Logs error after a counter reaches threshold; succesfull hall read should decrement it again.
*/
static inline void pxmc_rocon_process_hal_error(struct pxmc_state *mcs){
  if (mcs->pxms_halerc >= HAL_ERR_SENSITIVITY * HAL_ERR_MAX_COUNT){//log error only when error count exceeds treshold
    pxmc_set_errno(mcs, PXMS_E_HAL);
    mcs->pxms_ene = 0;
    mcs->pxms_halerc--;
    }
  else mcs->pxms_halerc += HAL_ERR_SENSITIVITY;//else just increment counter
}
/**
 * This appears to be the function that computes absolute position from hall sensors
*/
static inline void pxmc_pxmcc_pwm3ph_hal_and_ptofs_start_and_check(pxmc_state_t *mcs){
  short ptindx;
  short ptirc = mcs->pxms_ptirc;//irc count in phase table
  short divisor = mcs->pxms_ptper * 6;//periods in table * 6 = sections of HAL?
  unsigned char hal_pos;

     volatile pmsm_3pmdrv1_t *hw_axis = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst->hw_registers;
  hal_pos = pxmc_lpc_bdc_hal_pos_table[PMSM_3PMDRV_GET_HAL(hw_axis)];

  if (hal_pos == 0xff) {
    if (mcs->pxms_ene)pxmc_rocon_process_hal_error(mcs);
    } 
  else {
    if (mcs->pxms_halerc)mcs->pxms_halerc--;//error count decrement on success

    ptindx = (hal_pos * ptirc + divisor / 2) / divisor;//phase table index corresponding to HAL pos
    //if not update phase table index or realign phase table
    if (!(mcs->pxms_flg & PXMS_PTI_m) || (mcs->pxms_flg & PXMS_PRA_m)) {
        int set_ptofs_fl = 0;
        int ptofs_enable_update = 0;

        if (((hal_pos != mcs->pxms_hal) && (mcs->pxms_hal != 0x40)) && 1) {//hal value changed
            short ptindx_prev = (mcs->pxms_hal * ptirc + divisor / 2) / divisor;

            if ((ptindx > ptindx_prev + ptirc / 2) ||(ptindx_prev > ptindx + ptirc / 2)) {//moved by more than half of phase table between updates?
                ptindx = (ptindx_prev + ptindx - ptirc) / 2;
                if (ptindx < 0)ptindx += ptirc;
                } 
            else {
                ptindx = (ptindx_prev + ptindx) / 2;
                }

            set_ptofs_fl = 1;
            ptofs_enable_update = 1;

            pxmc_set_flag(mcs, PXMS_PTI_b);//allow update phase table index
            pxmc_clear_flag(mcs, PXMS_PRA_b);//claim that phase table is realigned
            } 
        else if (!(mcs->pxms_flg & PXMS_PTI_m))set_ptofs_fl = 1;       

        if (set_ptofs_fl) {
            mcs->pxms_ptindx = ptindx;
            mcs->pxms_ptofs = (mcs->pxms_ap >> PXMC_SUBDIV(mcs)) + mcs->pxms_ptshift - ptindx;

            pxmcc_pxmc_ptofs2mcc(mcs, ptofs_enable_update);
            }
        } 
    else {
        /* if phase table position to mask is know do fine phase table alignment */
        if (mcs->pxms_cfg & PXMS_CFG_I2PT_m) {//something something index mark
            int res;

            res = pxmc_inp_rocon_ptofs_from_index_poll(mcs, 0);
            if (res < 0) {
            pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
            } else if (res) {
            pxmcc_pxmc_ptofs2mcc(mcs, 1);
            pxmc_set_flag(mcs, PXMS_PTI_b);
            pxmc_set_flag(mcs, PXMS_PHA_b);
            }
        }
        }
    mcs->pxms_hal = hal_pos;
  }
}

/**
 * The function set as output callback in pxmc_state_t
*/
int pxmc_pxmcc_pwm3ph_out(pxmc_state_t *mcs){
  int32_t cur_d_raw;
  int32_t cur_q_raw;
  int ene, pwm_d, pwm_q;
  pxmc_z3pmdrv1_wmcc_state_t *mcsz3d1 = pxmc_state2z3pmdrv1_wmcc_state(mcs);
  z3pmdrv1_wmcc_state_t *adapter_state = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;

  //if not update index to phase table or not phase aligned or phase table realign
  if (!(mcs->pxms_flg & PXMS_PTI_m) || !(mcs->pxms_flg & PXMS_PHA_m) ||(mcs->pxms_flg & PXMS_PRA_m))
    pxmc_pxmcc_pwm3ph_hal_and_ptofs_start_and_check(mcs);
  //this is also called thru pxmc_pxmcc_cur_acquire_and_irc_inp when extended state
  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);

  {
    /*wind_current[0]=(ADC->ADDR0 & 0xFFF0)>>4;*/
    /* FIXME - check winding current against limit */
    /* pxmc_set_errno(mcs, PXMS_E_WINDCURRENT); */
  }

  ene = mcs->pxms_ene;
  pwm_d = 0;
  pwm_q = (PXMC_Z3PMDRV1_WMCC_PWM_CYCLE * ene) >> 15;

  if (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m)) {//if regulator or output only enabled
    int cur_d;
    int cur_d_req, cur_d_err;
    int max_pwm = (PXMC_Z3PMDRV1_WMCC_PWM_CYCLE * mcs->pxms_me) >> 15;

    cur_d_req = 0;
    //this is also called thru pxmc_pxmcc_cur_acquire_and_irc_inp when extended state
    pxmcc_axis_cur_dq_raw2filt(&cur_d, cur_d_raw);

   #ifdef PXMC_WITH_EXTENDED_STATE
    {
      int cur_q;
      pxmcc_axis_cur_dq_raw2filt(&cur_q, cur_q_raw);
      mcs->pxms_cur_d_act = cur_d;
      mcs->pxms_cur_q_act = cur_q;
    }
   #endif

    cur_d_err = cur_d_req - cur_d;

    if (!(mcs->pxms_flg & PXMS_ERR_m)) {
      /* Calculate D component current controller */
      pxmcc_cur_ctrl_pi(&pwm_d, &mcsz3d1->cur_d_err_sum, cur_d_err, mcsz3d1->cur_d_p, mcsz3d1->cur_d_i, max_pwm);
    }

    if (pxmc_rocon_rx_err_level >= 2)pxmc_set_errno(mcs, PXMS_E_WINDCURADC);
    else if (pxmc_rocon_mcc_stuck)pxmc_set_errno(mcs, PXMS_E_MCC_FAULT);
  } else {
    mcsz3d1->cur_d_err_sum = 0;
  }

  pxmcc_axis_pwm_dq_out(mcs, pwm_d, pwm_q);

  if (mcs->pxms_flg & PXMS_ERR_m){//if error, set pwm to 0 and hope coprocessor will not override
    volatile pmsm_3pmdrv1_t *hw = adapter_state->hw_registers;
    hw->pwm1=0;
    hw->pwm2=0;
    hw->pwm3=0;
  }

  return 0;
}
