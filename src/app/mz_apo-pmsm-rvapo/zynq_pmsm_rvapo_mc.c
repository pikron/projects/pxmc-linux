/*
  Communication with Zynq equipped by 3-phase
  motor driver connected to MicroZed on MZ_APO board.
  MZ_APO and 3-phase motor driver boards have been
  designed by Petr Porazil for PiKRON company.
  The Zynq VHDL design by Pavel Pisa, partially
  inspired by previous work done together with
  Martin Prudek.

  (C) 2017-2021 by Pavel Pisa ppisa@pikron.com
*/

#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <pxmc.h>

#include "mzapo_phys.h"
#include "out_pxmc.h"    //because of pxmcc_pxmc_ptofs2mcc
#include "pxmcc_types.h" //"mzapo_regs.h"
#include "appl_pxmc.h"
#include "zynq_pmsm_rvapo_mc.h"

char *memdev = "/dev/mem";
volatile pxmcc_data_t *mcc_data_global;
volatile void *rvapo_dmem_virt;
volatile void *rvapo_imem_virt;
volatile void *rvapo_ctrl_virt;

uint32_t firmware[]={
#include "firmware.inc" 
  0
};
/*Old function for hw access
static inline
uint32_t z3pmdrv1_wmcc_reg_rd(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst, unsigned reg_offs)
{
  return *(volatile uint32_t*)((char*)z3pmdrv1_wmccst->regs_base_virt + reg_offs);
}

static inline
void z3pmdrv1_wmcc_reg_wr(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst, unsigned reg_offs, uint32_t val)
{
  *(volatile uint32_t*)((char*)z3pmdrv1_wmccst->regs_base_virt + reg_offs) = val;
}

int z3pmdrv1_wmcc_irc_read(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst, uint32_t *irc_val)
{
  *irc_val = z3pmdrv1_wmccst->hw_registers->irc_pos;//z3pmdrv1_wmcc_reg_rd(z3pmdrv1_wmccst, DCSPDRV_REG_IRC_o);
  return 0;
}

void z3pmdrv1_wmcc_bidirpwm_set(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst, int32_t pwm_val)
{
  if (pwm_val > 0)
    z3pmdrv1_wmcc_reg_wr(z3pmdrv1_wmccst, DCSPDRV_REG_DUTY_o, pwm_val | DCSPDRV_REG_DUTY_DIR_A_m);
  else if (pwm_val < 0)
    z3pmdrv1_wmcc_reg_wr(z3pmdrv1_wmccst, DCSPDRV_REG_DUTY_o, -pwm_val | DCSPDRV_REG_DUTY_DIR_B_m);
  else
    z3pmdrv1_wmcc_reg_wr(z3pmdrv1_wmccst, DCSPDRV_REG_DUTY_o, 0);
}*/

/**
 *@return:  IRC value that is read by coprocessor.
 */
uint32_t pxmcc_read_irc_injectable(pxmc_state_t *mcs)
{
  pxmc_z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs);
  uint32_t irc_val = mcsrc->z3pmdrv1_wmccst->hw_registers->irc_pos;//*((volatile uint32_t *)(rvapo_dmem_virt + mcsrc->z3pmdrv1_wmccst->pxmcc_ctrl_struct->inp_info));
 // printf("IRC read: %08x from %08x + %08x\n",irc_val,(uint32_t)rvapo_dmem_virt,mcsrc->z3pmdrv1_wmccst->pxmcc_ctrl_struct->inp_info);
  return irc_val;
}
/**
 * Informs the coprocessor watchdog that the high level controller is still alive.
 * Force resets the watchdog if it ran out.
*/
void pxmcc_poke_watchdog(){
  if(mcc_data_global->common.watchdog_counter<0){
    printf("Watchdog underflow of %d, force resetting.\n",mcc_data_global->common.watchdog_counter);
    mcc_data_global->common.watchdog_counter=WATCHDOG_TIMEOUT;
    }
  mcc_data_global->common.watchdog_reset=WATCHDOG_RESET;
  }

/**
 * Enables the axis in coprocessor.
 */
void pxmcc_axis_enable(pxmc_state_t *mcs, int enable)
{
  pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst->pxmcc_ctrl_struct->ccflg = enable ? 1 : 0;
}

int pxmcc_axis_set_raw_pwm(pxmc_state_t *mcs, int channels, uint32_t *buf)
{
  z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
  volatile pmsm_3pmdrv1_t*hw_registers=mcsrc->hw_registers;
  volatile pxmcc_axis_data_t *mcc_axis = mcsrc->pxmcc_ctrl_struct;
  uint32_t pwmtx_info = mcc_axis->pwmtx_info;
  volatile uint32_t *uptr;

  mcc_axis->ccflg = 0;

  if (channels > 4)
    channels = 4;
  while (channels--) {
    uptr = (volatile uint32_t *)hw_registers + (pwmtx_info & 0xff);
    *uptr = *(buf++);
    pwmtx_info >>= 8;
  }
  return 0;
}

int pxmcc_axis_read_raw_pwm(pxmc_state_t *mcs, int channels, uint32_t *buf, uint32_t *p_ptindx)
{
  z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
  volatile pmsm_3pmdrv1_t*hw_registers=mcsrc->hw_registers;
  volatile pxmcc_axis_data_t *mcc_axis = mcsrc->pxmcc_ctrl_struct;
  uint32_t pwmtx_info = mcc_axis->pwmtx_info;
  volatile uint32_t *uptr;

  if (channels > 4)
    channels = 4;
  while (channels--) {
    uptr = (volatile uint32_t *)hw_registers + (pwmtx_info & 0xff);
    *(buf++) = *uptr;
    pwmtx_info >>= 8;
  }
  if (p_ptindx)
    *p_ptindx = mcc_axis->ptindx;
  return 0;
}

int pxmcc_axis_read_raw_adc(pxmc_state_t *mcs, int channels, uint32_t *buf, uint32_t *p_adc_done_sqn)
{
  z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
  volatile pmsm_3pmdrv1_t*hw_registers=mcsrc->hw_registers;
  volatile pxmcc_axis_data_t *mcc_axis = mcsrc->pxmcc_ctrl_struct;
  uint32_t out_info = mcc_axis->out_info;
  volatile uint32_t *uptr;
  int i;
  int try_count = 4;
  uint32_t sqn1, sqn2;

  if (channels > 4)
    channels = 4;

  while (try_count) {
    sqn1 = PMSM_3PMDRV_GET_ADCCNT(hw_registers);
    for (i = 0; i < channels; i++) {
      uptr = &(hw_registers->adc1) + out_info + i;
      buf[i] = *uptr;
    }
    sqn2 = PMSM_3PMDRV_GET_ADCCNT(hw_registers);
    if (sqn1 == sqn2) {
      if (p_adc_done_sqn)
        *p_adc_done_sqn = sqn1;
      return 0;
    }
  }

  return -1;
}

int pxmcc_axis_read_cur_dq_act(pxmc_state_t *mcs, int32_t *p_cur_d, int32_t *p_cur_q)
{
  z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
  volatile pxmcc_axis_data_t *mcc_axis = mcsrc->pxmcc_ctrl_struct;
  uint32_t cur_dq;
  cur_dq = mcc_axis->cur_dq;
  *p_cur_d = (int16_t)((cur_dq >> 16) & 0xffff);
  *p_cur_q = (int16_t)(cur_dq & 0xffff);
  return 0;
}

/**
 * Sets offset of adc (pxmcc_curadc_data_t::siroladc_offs) so that PWM=0 <=> ADC=0
 * Coprocessor is assumed to be disabled at time of call, so that pwm can be hard set to 0.
*/
int pxmcc_curadc_zero(pxmc_state_t *mcs, int wait)
{
  int chan;
//  unsigned try = wait ? 200 : 0;
//  volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
  volatile pxmcc_curadc_data_t *curadc;
  z3pmdrv1_wmcc_state_t *mcsrc = pxmc_state2z3pmdrv1_wmcc_state(mcs)->z3pmdrv1_wmccst;
  volatile pmsm_3pmdrv1_t*hw_registers=mcsrc->hw_registers;
  hw_registers->pwm1=0;//the important part here is to disable the pwm (enable bit is 30), duty is then irrelevant
  hw_registers->pwm2=0;
  hw_registers->pwm3=0;

  if (mcc_data_global->common.fwversion != PXMCC_FWVERSION)
    return -1;


/*  for (chan = 0; chan < PXMCC_CURADC_CHANNELS; chan++)
    pxmc_rocon_pwm_direct_wr(chan, 0, 0);

  do
  {
    if (mcc_data->common.fwversion == PXMCC_FWVERSION) //looks like wait for coprocessor init, do not need that
      break;
    if (!try--)
      return -1;
  } while (1);
  */

  /*
  if (wait)//wait for some hw master that we do not have here
  {
    if (pxmcc_wait_next_cycle() < 0)
      return -1;

    if (pxmcc_wait_next_cycle() < 0)
      return -1;
  }
  */

  for (chan = 0; chan < PXMCC_CURADC_CHANNELS; chan++)
  {
    curadc = mcc_data_global->curadc + chan;
    curadc->siroladc_offs += curadc->cur_val;
  }

  return 0;
}

/**
 * This coprocessor initialisation is copied straight from lx-rocon.
 */
int pxmcc_axis_setup(pxmc_state_t *mcs, int mode)
{
  volatile pxmcc_data_t *mcc_data = mcc_data_global;
  pxmc_z3pmdrv1_wmcc_state_t *mcsz3d1 = pxmc_state2z3pmdrv1_wmcc_state(mcs);
  z3pmdrv1_wmcc_state_t *mccst = mcsz3d1->z3pmdrv1_wmccst; // pxmc_state2rocon_state(mcs);
  volatile pxmcc_axis_data_t *mcc_axis = mccst->pxmcc_ctrl_struct;
  uint32_t ptirc;
  uint32_t ptreci;
  //  uint32_t inp_info;
  //  uint32_t pwmtx_info;
  //  uint32_t pwmtx_info_dummy = 27;
  uint64_t ull;
  //  int i;
  //  int phcnt = 0;
  //  int pwm_chan;

  if (mcc_axis == NULL)
    return -1;

  if (mcc_data->common.fwversion != PXMCC_FWVERSION)
    return -1;

  mcc_axis->ccflg = 0;
  mcc_axis->mode = PXMCC_MODE_IDLE;

  mcc_data->common.pwm_cycle = PXMC_Z3PMDRV1_WMCC_PWM_CYCLE;

  ptirc = mcs->pxms_ptirc;
  if (mode == PXMCC_MODE_STEPPER)
    ptirc <<= PXMC_SUBDIV(mcs);

  ull = (1ULL << 32) * mcs->pxms_ptper;
  ptreci = (ull + ptirc / 2) / ptirc;

  mcc_axis->ptreci = ptreci;

  pxmcc_pxmc_ptofs2mcc(mcs, 0);

  /* this is not necessary, the information is in pxmcc_types.pmsm_3pmdrv1_t which should be shared with firmware by symlink

    inp_info = (char *)&fpga_irc[mcs->pxms_inp_info]->count - (char *)fpga_irc[0];
    inp_info += mcc_data->common.irc_base;

    switch (mode)
    {
    case PXMCC_MODE_IDLE:
      phcnt = 0;
      break;
    case PXMCC_MODE_BLDC:
      phcnt = 3;
      break;
    case PXMCC_MODE_STEPPER_WITH_IRC:
      phcnt = 4;
      break;rvapo_dmem_virt
    case PXMCC_MODE_STEPPER:
      phcnt = 4;
      inp_info = (char *)&mcc_axis->steps_pos - (char *)mcc_data;
      break;
    case PXMCC_MODE_DC_CUR:
      phcnt = 2;
      inp_info = (char *)&mcc_axis->steps_pos - (char *)mcc_data;
      mcc_axis->ptreci = 0x1 << 24;
      break;
    }

    mcc_axis->inp_info = inp_info;
    mcc_axis->out_info = mcs->pxms_out_info;

    pwm_chan = mcs->pxms_out_info;

    pwmtx_info = (pwmtx_info_dummy << 0) | (pwmtx_info_dummy << 8) |
                 (pwmtx_info_dummy << 16) | (pwmtx_info_dummy << 24);

    for (i = phcnt; --i >= 0;)
    {
      volatile uint32_t *pwm_reg;
      volatile uint32_t *pwm_reg_base = fpga_lx_master_transmitter_base;

      pwmtx_info <<= 8;

      pwm_reg = pxmc_rocon_pwm_chan2reg(pwm_chan + i);
      if (pwm_reg == &pxmc_rocon_pwm_dummy_reg)
      {
        pwmtx_info |= pwmtx_info_dummy;
      }
      else
      {
        pwmtx_info |= pwm_reg - pwm_reg_base;
      }
    }

    mcc_axis->pwmtx_info = pwmtx_info;*/

  mcc_axis->mode = mode;

  mcc_axis->ccflg = 0;
  mcc_axis->pwm_dq = 0;

  pxmcc_curadc_zero(mcs, 1);

  if (mode == PXMCC_MODE_STEPPER)
  {
    mcsz3d1->steps_pos_prev = mcs->pxms_rp = mcs->pxms_ap;
    mcs->pxms_rs = mcs->pxms_as = 0;
    mcc_axis->steps_inc_next = 0;
    mcc_axis->steps_pos_next = mcsz3d1->steps_pos_prev;
    mcc_axis->steps_inc = 0;
    mcc_axis->steps_pos = mcsz3d1->steps_pos_prev;
    mcc_axis->ptirc = mcs->pxms_ptirc << PXMC_SUBDIV(mcs);
  }
  else if (mode == PXMCC_MODE_DC_CUR)
  {
    mcsz3d1->steps_pos_prev = mcs->pxms_rp = 0;
    mcs->pxms_rs = mcs->pxms_as = 0;
    mcc_axis->steps_inc_next = 0;
    mcc_axis->steps_pos_next = 0;
    mcc_axis->steps_inc = 0;
    mcc_axis->steps_pos = 0;
    mcc_axis->ptirc = 0xffffffff;
  }
  else
  {
    pxmcc_pxmc_ptofs2mcc(mcs, 1);
  }
  return 0;
}

int z3pmdrv1_wmcc_init(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst)
{
  int ret = 0;
  int try;
  int chan;
  // uint32_t irc_val;

  z3pmdrv1_wmccst->hw_registers = (pmsm_3pmdrv1_t *)map_phys_address(PMSM_3PMDRV1_BASE, PMSM_3PMDRV1_SIZE, 0);
  // IIRC map_phys_address only maps one page, rvapo dmem has two (8kB), so will fail if pxmcc_data_t ever gets placed too high
  // since .data is after .text, it means limit on code size
  // right now it is around 0x900, so there is not much space left
  rvapo_dmem_virt = map_phys_address(RVAPO_DMEM_BASE, RVAPO_DMEM_SIZE, 0);
  rvapo_imem_virt = map_phys_address(RVAPO_IMEM_BASE, RVAPO_IMEM_SIZE, 0);
  rvapo_ctrl_virt = map_phys_address(RVAPO_CTRL_BASE, RVAPO_CTRL_SIZE, 0);
  if (z3pmdrv1_wmccst->hw_registers == NULL || rvapo_dmem_virt == NULL ||
      rvapo_ctrl_virt == NULL || rvapo_imem_virt == NULL) {
    printf("ERROR: mapping of FPGA address regions failed\n");
    return -1;
  }

  printf("Stopping RVapo\n");

  RVAPO_RESET(rvapo_ctrl_virt);

  printf("RVapo stopped\n");

  /* Ensure that all PWM outputs are zeroed */
  asm ("dmb" : : : "memory");
  asm ("dsb" : : : "memory");
  z3pmdrv1_wmccst->hw_registers->pwm1 = 0;
  asm ("dmb" : : : "memory");
  asm ("dsb" : : : "memory");
  z3pmdrv1_wmccst->hw_registers->pwm2 = 0;
  asm ("dmb" : : : "memory");
  asm ("dsb" : : : "memory");
  z3pmdrv1_wmccst->hw_registers->pwm3 = 0;
  asm ("dmb" : : : "memory");
  asm ("dsb" : : : "memory");

  printf("Loading firmware\n");

  for(int i=0;i<(sizeof(firmware)/sizeof(firmware[0]))-1;i++){
    ((volatile uint32_t*)rvapo_imem_virt)[i]=firmware[i];
    ((volatile uint32_t*)rvapo_dmem_virt)[i]=firmware[i];
    }
  printf("phys %08x is virt %08"PRIxPTR"\n", RVAPO_DMEM_BASE, (uintptr_t)rvapo_dmem_virt);
  uint32_t pxmcc_data_addr_addr = ((uint32_t)rvapo_dmem_virt) + RVAPO_PXMCC_OFFSET;
  uint32_t pxmcc_data_addr = *((volatile uint32_t *)pxmcc_data_addr_addr);
  printf("pxmcc_data_t address: at %08x read %08x\n", pxmcc_data_addr_addr, pxmcc_data_addr);
  mcc_data_global = (pxmcc_data_t *)(rvapo_dmem_virt + (pxmcc_data_addr));
  z3pmdrv1_wmccst->pxmcc_ctrl_struct = &(mcc_data_global->axis[0]); // TODO what if there are more axes?


  z3pmdrv1_wmccst->pos_offset = -z3pmdrv1_wmccst->hw_registers->irc_pos;

  //uint32_t irc_val = *((volatile uint32_t *)(rvapo_dmem_virt + mcc_data_global->axis[0].inp_info));
  //printf("IRC read: %08x from %08x + %08x\n",irc_val,(uint32_t)rvapo_dmem_virt,mcc_data_global->axis[0].inp_info);
  uint32_t irc_val = z3pmdrv1_wmccst->hw_registers->irc_pos;
  printf("IRC read: %08x\n",irc_val);
  printf("dmem start:\n");
  for(int i=0;i<32;i+=4)printf("%08x ",*((volatile uint32_t*)(rvapo_dmem_virt+i)));
  printf("\n");

  RVAPO_RUN(rvapo_ctrl_virt);

  try = 10000;
  do {
    asm ("dmb" : : : "memory");
    asm ("dsb" : : : "memory");
     /* wait for coprocesor to initialize */
    if (mcc_data_global->common.fwversion != 0)
      break;
    if (!try--)
      return -1;
  } while (1);

  printf("version: %08x\n",mcc_data_global->common.fwversion);
  if (mcc_data_global->common.fwversion != PXMCC_FWVERSION) {
    printf("ERROR: version mismatch, expected %08x\n", PXMCC_FWVERSION);
  }

  sleep(1);
  for (chan = 0; chan < PXMCC_CURADC_CHANNELS; chan++) {
    volatile pxmcc_curadc_data_t *curadc;
    curadc = mcc_data_global->curadc + chan;
    curadc->siroladc_offs += curadc->cur_val;
    printf("adc %d offset 0x%08x\n", chan, curadc->siroladc_offs);
  }

  mcc_data_global->axis[0].mode = PXMCC_MODE_BLDC;
  mcc_data_global->axis[0].ccflg = 1;
  return ret;
}
