/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmccmds.c - position controller RoCoN specific commands

  Copyright (C) 2001-2013 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2013 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>

#include <utils.h>

#include "pxmc_cmds.h"

#include "appl_defs.h"
#include "appl_pxmc.h"

#include "inp_pxmc.h"
#include "zynq_pmsm_rvapo_mc.h"

#define Z3D1WMCC_LOG_CURRENT_SIZE 1024*1024
#define Z3D1WMCC_ADC_CHAN_COUNT 3
#define Z3PMDRV1_PWM_EN     0x40000000
#define Z3PMDRV1_PWM_SHDN   0x80000000

int32_t *z3d1wmcc_logcurrent_buff;
int32_t *z3d1wmcc_logcurrent_pos;
int z3d1wmcc_logcurrent_mode;

int z3d1wmcc_logcurrent(pxmc_state_t *mcs)
{
  uint32_t pwm[3];
  uint32_t adc[3];
  uint32_t ptindx;
  uint32_t adc_done_sqn;

  static uint32_t adc_done_sqn_last;
  static uint32_t adc_last[3];

  if ((z3d1wmcc_logcurrent_buff == NULL) ||
      (z3d1wmcc_logcurrent_pos == NULL) ||
      ((char*)z3d1wmcc_logcurrent_pos -
       (char*)z3d1wmcc_logcurrent_buff + 64 >= Z3D1WMCC_LOG_CURRENT_SIZE))
    return 0;

  pxmcc_axis_read_raw_pwm(mcs, 3, pwm, &ptindx);

  z3d1wmcc_logcurrent_pos[0] = ptindx;

  z3d1wmcc_logcurrent_pos[1] = pwm[0];
  z3d1wmcc_logcurrent_pos[2] = pwm[1];
  z3d1wmcc_logcurrent_pos[3] = pwm[2];

  if (z3d1wmcc_logcurrent_mode == 1) {
    z3d1wmcc_logcurrent_pos[4] = 1;
   #if 0
   #ifdef PXMC_WITH_EXTENDED_STATE
    z3d1wmcc_logcurrent_pos[5] = mcs->pxms_cur_d_act;
    z3d1wmcc_logcurrent_pos[6] = mcs->pxms_cur_q_act;
   #endif /*PXMC_WITH_EXTENDED_STATE*/
   #else
    {
      int32_t cur_d;
      int32_t cur_q;
      pxmcc_axis_read_cur_dq_act(mcs, &cur_d, &cur_q);
      z3d1wmcc_logcurrent_pos[5] = cur_d;
      z3d1wmcc_logcurrent_pos[6] = cur_q;
    }
   #endif
    z3d1wmcc_logcurrent_pos[7] = 0;
  } else {
    pxmcc_axis_read_raw_adc(mcs, 3, adc, &adc_done_sqn);
    z3d1wmcc_logcurrent_pos[4] = (adc_done_sqn - adc_done_sqn_last) & 0xfff;
    adc_done_sqn_last = adc_done_sqn;
    z3d1wmcc_logcurrent_pos[5] = (adc[0] - adc_last[0]) & 0xffffff;
    z3d1wmcc_logcurrent_pos[6] = (adc[1] - adc_last[1]) & 0xffffff;
    z3d1wmcc_logcurrent_pos[7] = (adc[2] - adc_last[2]) & 0xffffff;
    adc_last[0] = adc[0];
    adc_last[1] = adc[1];
    adc_last[2] = adc[2];
  }

  z3d1wmcc_logcurrent_pos += 8;

  return 0;
}

int cmd_do_logcurrent(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  int fd;
  size_t log_size;
  long log_mode = 0;

  if (ps && (si_skspace(&ps), *ps)) {
    if (si_long(&ps, &log_mode, 0) < 0)
      return -CMDERR_BADPAR;
  }

  if (pxmc_main_list.pxml_cnt < 1)
    return -1;

  if (pxmc_dbgset(pxmc_main_list.pxml_arr[0], NULL, 0) < 0)
    return -1;

  if (z3d1wmcc_logcurrent_buff == NULL) {
    z3d1wmcc_logcurrent_buff = malloc(Z3D1WMCC_LOG_CURRENT_SIZE);
    if (z3d1wmcc_logcurrent_buff == NULL)
      return -1;
  }

  if (z3d1wmcc_logcurrent_pos != NULL) {
    log_size = (char*)z3d1wmcc_logcurrent_pos - (char*)z3d1wmcc_logcurrent_buff;
    printf("Log size %ld\n", (long)log_size);

    if ((z3d1wmcc_logcurrent_pos > z3d1wmcc_logcurrent_buff) &&
        (log_size < Z3D1WMCC_LOG_CURRENT_SIZE)) {

      fd = open("currents.bin", O_WRONLY | O_CREAT | O_TRUNC,
              S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

      if (fd == -1)
        return -1;

      write(fd, z3d1wmcc_logcurrent_buff, log_size);

      close(fd);
    }
  }

  z3d1wmcc_logcurrent_pos = z3d1wmcc_logcurrent_buff;
  z3d1wmcc_logcurrent_mode = log_mode;

  if (pxmc_dbgset(pxmc_main_list.pxml_arr[0], z3d1wmcc_logcurrent, 1) < 0)
    return -1;

  return 0;
}


typedef struct z3d1wmcc_currentcal_state_t {
  int req_accum;
  int accum_cnt;
  uint32_t curadc_sqn_last;
  uint64_t curadc_last[Z3D1WMCC_ADC_CHAN_COUNT];
  uint64_t curadc_accum[Z3D1WMCC_ADC_CHAN_COUNT];
  int pwm_channels;
  uint32_t pwm[4];
} z3d1wmcc_currentcal_state_t;

z3d1wmcc_currentcal_state_t z3d1wmcc_currentcal_state;
sem_t z3d1wmcc_currentcal_sem;

int z3d1wmcc_currentcal_accum(pxmc_state_t *mcs)
{
  /*pxmc_z3d1wmcc_state_t *mcsrc = pxmc_state2z3d1wmcc_state(mcs); */
  uint32_t curadc_sqn_diff;
  uint32_t curadc_val_diff;
  uint32_t curadc_done_sqn;
  uint32_t curadc[3];
  int i;
  z3d1wmcc_currentcal_state_t *cucalst = &z3d1wmcc_currentcal_state;

  if (cucalst->pwm_channels)
    pxmcc_axis_set_raw_pwm(mcs, cucalst->pwm_channels, cucalst->pwm);

  if (cucalst->accum_cnt >= cucalst->req_accum)
    return 0;

  pxmcc_axis_read_raw_adc(mcs, 3, curadc, &curadc_done_sqn);

  if (cucalst->accum_cnt < 0) {
    cucalst->accum_cnt = 0;
    for (i = 0; i < Z3D1WMCC_ADC_CHAN_COUNT; i++)
      cucalst->curadc_accum[i] = 0;
  } else {
    curadc_sqn_diff = curadc_done_sqn - cucalst->curadc_sqn_last;
    cucalst->curadc_sqn_last = curadc_done_sqn;
    curadc_sqn_diff &= 0x1ff;

    cucalst->accum_cnt += curadc_sqn_diff;

    for (i = 0; i < Z3D1WMCC_ADC_CHAN_COUNT; i++) {
      curadc_val_diff = curadc[i] - cucalst->curadc_last[i];
      cucalst->curadc_accum[i] += curadc_val_diff & 0x00ffffff;
    }
  }
  for (i = 0; i < Z3D1WMCC_ADC_CHAN_COUNT; i++) {
    cucalst->curadc_last[i] = curadc[i];
  }
  cucalst->curadc_sqn_last = curadc_done_sqn;

  if (cucalst->accum_cnt >= cucalst->req_accum)
    sem_post(&z3d1wmcc_currentcal_sem);

  if (z3d1wmcc_logcurrent_buff != NULL)
    z3d1wmcc_logcurrent(mcs);

  return 0;
}

int z3d1wmcc_currentcal_setup(pxmc_state_t *mcs, z3d1wmcc_currentcal_state_t *cucalst,
                           unsigned int req_accum,
                           unsigned int pwm1, int pwm1_en,
                           unsigned int pwm2, int pwm2_en,
                           unsigned int pwm3, int pwm3_en)
{
  int i;

  cucalst->pwm[0] = pwm1 | (pwm1_en? Z3PMDRV1_PWM_EN: Z3PMDRV1_PWM_SHDN);
  cucalst->pwm[1] = pwm2 | (pwm2_en? Z3PMDRV1_PWM_EN: Z3PMDRV1_PWM_SHDN);
  cucalst->pwm[2] = pwm3 | (pwm3_en? Z3PMDRV1_PWM_EN: Z3PMDRV1_PWM_SHDN);
  cucalst->pwm_channels = 3;
  __memory_barrier();

  pxmcc_axis_set_raw_pwm(mcs, cucalst->pwm_channels, cucalst->pwm);
  __memory_barrier();

  for (i = 0; i < Z3D1WMCC_ADC_CHAN_COUNT; i++)
    cucalst->curadc_accum[i] = 0;

  cucalst->req_accum = req_accum;
  __memory_barrier();
  cucalst->accum_cnt = -1;
  __memory_barrier();

  return 0;
}

int z3d1wmcc_currentcal_save_to_csv(int curr_meas_cnt, double curr_meas[curr_meas_cnt][3], char *filename)
{
  FILE *f = fopen(filename, "w");
  if (f == NULL)
    return -1;

  printf("Entering pxmc_curr_to_csv\n");

  for (int i = 0; i < curr_meas_cnt; i++)
  {
    fprintf(f, "%f, %f, %f\n", curr_meas[i][0], curr_meas[i][1], curr_meas[i][2]);
    printf("%f, %f, %f\n", curr_meas[i][0], curr_meas[i][1], curr_meas[i][2]);
  }

  fclose(f);
  return 0;
}

int z3d1wmcc_currentcal_pattern[7][6] = {
 /* PWM1, EN1,  PWM2, EN2,  PWM3, EN3 */
  {  0,    0,    0,    0,    0,    0},
  {  1,    1,    0,    1,    0,    0},
  {  0,    1,    1,    1,    0,    0},
  {  0,    0,    1,    1,    0,    1},
  {  0,    0,    0,    1,    1,    1},
  {  0,    1,    0,    0,    1,    1},
  {  1,    1,    0,    0,    0,    1},
};

#define CURRENTCAL_PATTERN_ROWS \
  (sizeof(z3d1wmcc_currentcal_pattern)/sizeof(z3d1wmcc_currentcal_pattern[0]))

int cmd_do_currentcal(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  pxmc_state_t *mcs;
  long pwm;
  z3d1wmcc_currentcal_state_t *cucalst = &z3d1wmcc_currentcal_state;
  unsigned int req_accum = 30000;
  unsigned int skip_accum = 10000;
  int cycle;
  /* 7 cycles (depending on len of z3d1wmcc_currentcal_pattern), 3 phases */
  double curr_meas[CURRENTCAL_PATTERN_ROWS][3];
  char filename [50];

  if (!ps || (si_skspace(&ps), !*ps))
    return -CMDERR_BADPAR;

  if (si_long(&ps, &pwm, 0) < 0)
    return -CMDERR_BADPAR;

  if (pxmc_main_list.pxml_cnt < 1)
    return -1;

  mcs = pxmc_main_list.pxml_arr[0];
  if (mcs == NULL)
    return -1;

  pxmc_axis_release(mcs);

  if (pxmc_dbgset(mcs, NULL, 0) < 0)
    return -1;

  if (sem_init(&z3d1wmcc_currentcal_sem, 0, 0))
    return -1;

  printf("Starting current calibration\n");

  for (cycle = 0; cycle < CURRENTCAL_PATTERN_ROWS; cycle++)
  {
    int *p = z3d1wmcc_currentcal_pattern[cycle];
    unsigned int pwm1 = pwm * p[0];
    int pwm1_en = p[1];
    unsigned int pwm2 = pwm * p[2];
    int pwm2_en = p[3];
    unsigned int pwm3 = pwm * p[4];
    int pwm3_en = p[5];

    /* clear debug routine and disable debug */
    pxmc_dbgset(mcs, NULL, 0);

    /*
     * setup new debug routine, at each sampling period do cucalst,
     * for skip_accum number of times, just to wait for stabilisation
     */
    z3d1wmcc_currentcal_setup(mcs, cucalst, skip_accum,
                           pwm1, pwm1_en, pwm2, pwm2_en, pwm3, pwm3_en);

    /* register the previously setup debug routine */
    pxmc_dbgset(mcs, z3d1wmcc_currentcal_accum, 1);
    sem_wait(&z3d1wmcc_currentcal_sem);

    printf("cycle %d - press enter to record and continue by next one\n", cycle);
    /* wait for user confirmation */
    char buff[10];
    fgets(buff, 9, stdin);

    /*
     * setup new debug routine, at each sampling period do cucalst,
     * for req_accum number of times
     */
    z3d1wmcc_currentcal_setup(mcs, cucalst, req_accum,
                           pwm1, pwm1_en, pwm2, pwm2_en, pwm3, pwm3_en);
    pxmc_dbgset(mcs, z3d1wmcc_currentcal_accum, 1);
    sem_wait(&z3d1wmcc_currentcal_sem);

    pxmc_dbgset(mcs, NULL, 0);

    printf("%4u %d %4u %d %4u %d   %f %f %f\n",
           pwm1, pwm1_en, pwm2, pwm2_en, pwm3, pwm3_en,
           (double)cucalst->curadc_accum[0] / cucalst->accum_cnt,
           (double)cucalst->curadc_accum[1] / cucalst->accum_cnt,
           (double)cucalst->curadc_accum[2] / cucalst->accum_cnt);

    /* store the measured currents into array, to be saved into csv file */
    for (int i = 0; i < 3; i++) {
      curr_meas[cycle][i] = (double)cucalst->curadc_accum[i] / cucalst->accum_cnt;
    }
  }

  snprintf(filename, sizeof(filename), "curr_meas_%ld.csv", pwm);
  z3d1wmcc_currentcal_save_to_csv(CURRENTCAL_PATTERN_ROWS, curr_meas, filename);
  printf("Data stored to %s\n", filename);

  pxmc_dbgset(mcs, NULL, 0);

  sem_destroy(&z3d1wmcc_currentcal_sem);

  pxmc_axis_release(mcs);

  pxmcc_axis_enable(mcs, 1);

  return 0;
}

/**
 * cmd_do_axis_mode - checks the command format and busy flag validity, calls pxmc_axis_mode
 *
 * if pxmc_axis_mode returns -1, cmd_do_axis_mode returns -1.
 */
int cmd_do_axis_mode(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  pxmc_state_t *mcs;

  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  if(*param[2]=='?') {
    return cmd_opchar_replong(cmd_io, param, pxmc_axis_rdmode(mcs), 0, 0);
  }

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(mcs->pxms_flg&PXMS_BSY_m) return -CMDERR_BSYREG;

  val=atol(param[3]);
  val=pxmc_axis_mode(mcs,val);
  if(val<0)
    return val;

  return 0;
}

/**
 * cmd_do_regsfrq - sets or returns smapling frequency
 */
#ifdef WITH_SFI_SEL
int cmd_do_regsfrq(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    val=atoi(param[3]);
    return pxmc_sfi_sel(val);
  }else{
    return cmd_opchar_replong(cmd_io, param, pxmc_get_sfi_hz(NULL), 0, 0);
  }
  return 0;
}
#endif /* WITH_SFI_SEL */

cmd_des_t const cmd_des_regcurdp={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURDP?","current controller d component p parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_z3pmdrv1_wmcc_state_offs(cur_d_p),
                         0}};

cmd_des_t const cmd_des_regcurdi={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURDI?","current controller d component i parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_z3pmdrv1_wmcc_state_offs(cur_d_i),
                         0}};

cmd_des_t const cmd_des_regcurqp={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURQP?","current controller q component p parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_z3pmdrv1_wmcc_state_offs(cur_q_p),
                         0}};

cmd_des_t const cmd_des_regcurqi={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURQI?","current controller q component i parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_z3pmdrv1_wmcc_state_offs(cur_q_i),
                         0}};

cmd_des_t const cmd_des_regcurhold={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURHOLD?","current steady hold value for stepper", cmd_do_reg_short_val,
                        {(char*)pxmc_z3pmdrv1_wmcc_state_offs(cur_hold),
                         0}};

cmd_des_t const cmd_des_axis_mode={0, CDESM_OPCHR|CDESM_WR,
                        "REGMODE?","axis working mode",cmd_do_axis_mode,
                         {}};

#ifdef WITH_SFI_SEL
cmd_des_t const cmd_des_regsfrq={0, CDESM_OPCHR|CDESM_RW,
                        "REGSFRQ","set new sampling frequency",cmd_do_regsfrq,{}};
#endif /* WITH_SFI_SEL */

cmd_des_t const cmd_des_logcurrent={0, 0,
                        "logcurrent","log current history", cmd_do_logcurrent,
                        {(char*)0,
                         0}};

cmd_des_t const cmd_des_currentcal={0, 0,
                        "currentcal","current calibration", cmd_do_currentcal,
                        {(char*)0,
                         0}};

cmd_des_t const *cmd_appl_pxmc[] =
{
  &cmd_des_regcurdp,
  &cmd_des_regcurdi,
  &cmd_des_regcurqp,
  &cmd_des_regcurqi,
  &cmd_des_regcurhold,
  &cmd_des_axis_mode,
#ifdef WITH_SFI_SEL
  &cmd_des_regsfrq,
#endif /* WITH_SFI_SEL */
  &cmd_des_logcurrent,
  &cmd_des_currentcal,
  NULL
};
