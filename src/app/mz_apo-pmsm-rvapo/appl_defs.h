#ifndef _TEST_LPC_H
#define _TEST_LPC_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include "appl_config.h"

#ifdef CONFIG_OC_APP_ZYNQ_TEMPLATE_WITH_BSDNET
#define APPL_WITH_BSDNET
#endif

#define APP_VER_ID "pxmc-test-zynq-rvapo"

#define SHELL_TASK_PRIORITY 50

/*
#define APPL_RUN_AT_MAIN_LOOP do { \
   pxmc_coordmv_process(); \
 } while(0)
*/

int pxmc_initialize(void);

int pxmc_done(void);

int main(int argc, char **argv);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _TEST_LPC_H */

