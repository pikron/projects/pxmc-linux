/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmcc_types.h - multi axis motion controller coprocessor
        for FPGA tumble CPU of lx-rocon system - data types

  (C) 2001-2014 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2014 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMCC_TYPES_H_
#define _PXMCC_TYPES_H_

#include <stdint.h>

#define WATCHDOG_TIMEOUT      100
#define WATCHDOG_RESET        0xAABB55
#define PXMCC_FWVERSION       0xACCE0003
#define PXMCC_AXIS_COUNT      1
#define PXMCC_CURADC_CHANNELS 3

#define PXMCC_MODE_IDLE              2
#define PXMCC_MODE_BLDC              0
#define PXMCC_MODE_STEPPER_WITH_IRC  1
#define PXMCC_MODE_STEPPER           3
#define PXMCC_MODE_DC_CUR            4

typedef struct pmsm_3pmdrv1_t{
	uint32_t word0;
	uint32_t word1;
	uint32_t irc_pos;//The actual irc value. Read only.
	uint32_t irc_idx_pos;//Also irc value, but updated only when irc_idx='1', whatever it may be.
	uint32_t pwm1;
	uint32_t pwm2;
	uint32_t pwm3;
	uint32_t word7;
	uint32_t status;//bit 0-11 is adc conversions count,bit 12 is pwm desync, bit 16-18 is HAL, bit 20-22 is pwm status, 24 is power status.
	uint32_t adc1;
	uint32_t adc2;
	uint32_t adc3;
} pmsm_3pmdrv1_t;

#define PMSM_3PMDRV_GET_ADCCNT(a)	((a)->status&0xFFF)
#define PMSM_3PMDRV_GET_HAL(a)		(((a)->status>>16)&0x7)

typedef volatile struct pxmcc_common_data_t {
  uint32_t  fwversion;
  uint32_t  pwm_cycle;
  uint32_t  act_idle;
  uint32_t  min_idle;//shortest time waiting for sync, probably to ensure there is enough time
  uint32_t  rx_done_sqn;
  uint32_t  rx_err_cnt;
  uint32_t  irc_base;//this is never used
  uint32_t  watchdog_reset;
  int32_t   watchdog_counter;
  uint32_t  trace_address;//address to save traces at
  uint32_t  trace_size;//size of allocate space for traces
} pxmcc_common_data_t;

typedef volatile struct pxmcc_axis_data_t {
  uint32_t  ccflg;  /* set this to not 0 0=0x0 bytes*/
  uint32_t  mode;
  uint32_t  pwm_dq;	/* D and Q components of PWM (pwm_d << 16) | (pwm_q) & 0xffff, input*///2=0x8, 24 total
  uint32_t  cur_dq;	/* D and Q components current (cur_d << 16) | (cur_q) & 0xffff, feedback */
  uint32_t  ptindx;	/* index into phase table (pt is phase table) / irc in the cycle */
  uint32_t  ptirc;	/* IRC count per phase table */
  uint32_t  ptreci;	/* Reciprocal value of ptirc * 63356  */
  uint32_t  ptofs;	/* offset between table and IRC counter */
  int32_t   ptsin;
  int32_t   ptcos;
  uint32_t  ptphs;
  uint32_t  cur_d_cum;
  uint32_t  cur_q_cum;
  uint32_t  inp_info;	/* which irc to use (this is the address that will actually be used, not the one in tumbl_addr.h) */
  uint32_t  out_info;	/* output index, indexes the curadc array, since three of its elems belong to one axis*/
  uint32_t  pwmtx_info;	/* offsets of pwm1 .. pwm4 from FPGA_LX_MASTER_TX (lowest byte is offset of PWM1 in word etc.) */
  uint32_t  pwm_prew[4];  /*previous values of pwm sent to motor*/
  uint32_t  steps_inc;	/* increments for selfgenerated stepper motor */
  uint32_t  steps_pos;	/* self generated position for stepper motor 21=0x0x54,0x70 total*/
  uint32_t  steps_sqn_next; /* when to apply steps_inc_next */
  uint32_t  steps_inc_next; /* increment to apply at steps_sqn_next */
  uint32_t  steps_pos_next; /* base position to apply at steps_sqn_next */
  int32_t   cur_cal_matrix[2][2]; /* current calibration matrix for alpha, beta system */
} pxmcc_axis_data_t;

typedef struct pxmcc_curadc_data_t {
  int32_t   cur_val;//offset diff
  int32_t   siroladc_offs;
  uint32_t  siroladc_last;//raw ADC value
} pxmcc_curadc_data_t;

typedef volatile struct pxmcc_data_t {
  pxmcc_common_data_t common;//size is 7 words = 0x1C bytes
  pxmcc_axis_data_t   axis[PXMCC_AXIS_COUNT];//25 words = 0x64 bytes
  pxmcc_curadc_data_t curadc[PXMCC_CURADC_CHANNELS];  //3 words = 0xC bytes
} pxmcc_data_t;

#endif /*_PXMCC_TYPES_H_*/
