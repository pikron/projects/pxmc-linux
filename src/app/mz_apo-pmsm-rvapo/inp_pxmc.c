/**
 * Communication with Zynq equipped by 3-phase
 * motor driver connected to MicroZed on MZ_APO board.
 * Specifically, this is the MZ_APO -> motor part.
 * Most of this code is copied from lx-rocon project:
 * https://gitlab.com/pikron/projects/lx_cpu/lx-rocon/-/blob/master/sw/app/rocon/appl_pxmc.c?ref_type=heads
*/
#include <stdio.h>
#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
//#include <pxmc_lpc_qei.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include <pxmc_gen_info.h>
#include <stdlib.h>
#include <string.h>

#include "appl_defs.h"
#include "appl_pxmc.h"
#include "appl_pxmc.h"
#include "pxmcc_types.h"
#include "zynq_pmsm_rvapo_mc.h"
//#include "pxmcc_interface.h"
#include "inp_pxmc.h"

/**
 *The basic input reader, takes only irc. Practically same as lx-rocon's pxmc_inp_rocon_inp, just different way to read irc
*/
int pxmc_inp_z3pmdrv1_wmcc_inp(struct pxmc_state *mcs)
{
  pxmc_z3pmdrv1_wmcc_state_t *mcsz3d1 = pxmc_state2z3pmdrv1_wmcc_state(mcs);
  /*int chan=mcs->pxms_inp_info;*/
  uint32_t irc_val=pxmcc_read_irc_injectable(mcs);//*((uint32_t*)mcsrc->z3pmdrv1_wmccst->pxmcc_ctrl_struct->inp_info);
  long irc;
  long pos;

  irc_val += mcsz3d1->z3pmdrv1_wmccst->pos_offset;
  irc = irc_val;

  pos = irc << PXMC_SUBDIV(mcs);
  mcs->pxms_as = pos - mcs->pxms_ap;
  mcs->pxms_ap = pos;

  /* Running of the motor commutator */
  if (mcs->pxms_flg & PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs, irc);//test if this should really be done

  return 0;
}

void pxmcc_axis_get_cur_dq_filt_raw(pxmc_state_t *mcs,int32_t *p_cur_d_raw, int32_t *p_cur_q_raw){
  pxmc_z3pmdrv1_wmcc_state_t *mcsz3d1 = pxmc_state2z3pmdrv1_wmcc_state(mcs);
  volatile z3pmdrv1_wmcc_state_t *adapter_state= mcsz3d1->z3pmdrv1_wmccst;

  uint32_t cur_d_cum = adapter_state->pxmcc_ctrl_struct->cur_d_cum;
  uint32_t cur_q_cum = adapter_state->pxmcc_ctrl_struct->cur_q_cum;
  int32_t cur_d;
  int32_t cur_q;

  cur_d = cur_d_cum - mcsz3d1->cur_d_cum_prev;
  mcsz3d1->cur_d_cum_prev = cur_d_cum;
  cur_q = cur_q_cum - mcsz3d1->cur_q_cum_prev;
  mcsz3d1->cur_q_cum_prev = cur_q_cum;

  *p_cur_d_raw = cur_d;
  *p_cur_q_raw = cur_q;
}

void pxmcc_axis_cur_dq_raw2filt(int *p_cur, int32_t cur_raw){
  int cur_div;
  int32_t cur;
  cur_div = cur_raw & 0x1f;
  if (cur_div) {
    cur = cur_raw / cur_div;
    cur += (1 << 11) | 0x20;
    *p_cur = cur >> 12;
  }
}


void pxmcc_axis_get_cur_dq_filt(pxmc_state_t *mcs, int *p_cur_d, int *p_cur_q)
{
  int32_t cur_d_raw;
  int32_t cur_q_raw;

  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);

  pxmcc_axis_cur_dq_raw2filt(p_cur_d, cur_d_raw);
  pxmcc_axis_cur_dq_raw2filt(p_cur_q, cur_q_raw);
}

#ifdef PXMC_WITH_EXTENDED_STATE
int
pxmc_pxmcc_cur_acquire_and_irc_inp(pxmc_state_t *mcs)
{
  int cur_d;
  int cur_q;

  pxmc_inp_z3pmdrv1_wmcc_inp(mcs);
  pxmcc_axis_get_cur_dq_filt(mcs, &cur_d, &cur_q);
  mcs->pxms_cur_d_act = cur_d;
  mcs->pxms_cur_q_act = cur_q;

  return 0;
}
#endif /*PXMC_WITH_EXTENDED_STATE*/
