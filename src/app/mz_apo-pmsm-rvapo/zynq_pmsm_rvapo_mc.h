#ifndef _ZYNQ_Z3PMDRV1_WMCC_MC_H
#define _ZYNQ_Z3PMDRV1_WMCC_MC_H

#include <stdint.h>

#define PXMC_Z3PMDRV1_WMCC_PWM_CYCLE 5000
#define PMSM_3PMDRV1_BASE       0x43c20000
#define PMSM_3PMDRV1_SIZE       0x40
#define RVAPO_CTRL_BASE         0x43c00000 // 0x43c40000
#define RVAPO_CTRL_SIZE         64
#define RVAPO_RESET(a)          ((*(volatile uint32_t*)a)|=1)    //a is virtual address of RVAPO_CTRL_BASE
#define RVAPO_RUN(a)            ((*(volatile uint32_t*)a)&=(~1))
#define RVAPO_DMEM_BASE         0x42000000
#define RVAPO_DMEM_SIZE         0x2000
#define RVAPO_IMEM_BASE         0x40000000
#define RVAPO_IMEM_SIZE         0x2000
/* //the firmware linker script will put address of pxmcc_types.pxmcc_data_t here. */
#define RVAPO_PXMCC_OFFSET      0x10

struct pmsm_3pmdrv1_t;
struct pxmcc_axis_data_t;

typedef struct z3pmdrv1_wmcc_state_t {
  /* volatile has to be here so that the memory pointed to is volatile, not the struct itself */
  volatile struct pmsm_3pmdrv1_t *hw_registers;
  volatile struct pxmcc_axis_data_t *pxmcc_ctrl_struct;
  uint32_t  pos_offset;
} z3pmdrv1_wmcc_state_t;

int z3pmdrv1_wmcc_init(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst);

//int z3pmdrv1_wmcc_irc_read(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst, uint32_t *irc_val);

int pxmcc_axis_setup(pxmc_state_t *mcs, int mode);
void pxmcc_axis_enable(pxmc_state_t *mcs, int enable);
uint32_t pxmcc_read_irc_injectable(pxmc_state_t*mcs);
void pxmcc_poke_watchdog();
int pxmcc_axis_set_raw_pwm(pxmc_state_t *mcs, int channels, uint32_t *buf);
int pxmcc_axis_read_raw_pwm(pxmc_state_t *mcs, int channels, uint32_t *buf, uint32_t *p_ptindx);
int pxmcc_axis_read_raw_adc(pxmc_state_t *mcs, int channels, uint32_t *buf, uint32_t *p_adc_done_sqn);
int pxmcc_axis_read_cur_dq_act(pxmc_state_t *mcs, int32_t *p_cur_d, int32_t *p_cur_q);

//void z3pmdrv1_wmcc_bidirpwm_set(z3pmdrv1_wmcc_state_t *z3pmdrv1_wmccst, int32_t pwm_val);

#endif /*_ZYNQ_Z3PMDRV1_WMCC_MC_H*/
