#include <system_def.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "appl_defs.h"
#include "appl_utils.h"
#include "pxmcc_types.h"
//#include "zynq_pmsm_rvapo_mc.h"

void appl_stop(void)
{
  pxmc_done();
  fprintf(stderr, "Application abnormal termination\n");
  sleep(1);
  /* stop clock pin driving FPGA to ensure failase state */
}

/***********************************/
int main(int argc, char *argv[])
{
  //Application setup: priority, exit and signal handlers
  appl_setup_environment(argv[0]);
  //initialise the hardware side
  pxmc_initialize();

  do {
    cmdproc_poll();//poll commands. How are they sent to controller?
  } while(1);

  return 0;
}

