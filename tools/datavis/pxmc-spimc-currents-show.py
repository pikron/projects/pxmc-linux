#! /usr/bin/python

import numpy as np
import math
#import matplotlib as plt
#from numpy import cos
from pylab import plot, subplot, figure, show, ylim, yticks
from pprint import pprint
import argparse

class spimc_state(object):
  def __init__(self, data_fname = None, adc_channels = 3, pwm_channels = 3,
               dmem_words = 0, skip_words = 0, period = 2500, adc_oversample = 1,
               filtspan = 1, isr_divider = 1, samples = None, do_curcal = False):
    self.adc_channels = adc_channels
    self.pwm_channels = pwm_channels
    self.period = period
    self.adc_oversample = adc_oversample
    self.isr_divider = isr_divider
    self.filtspan = filtspan
    self.dmem_words = dmem_words
    self.adc_channels = adc_channels
    self.pwm_channels = pwm_channels
    self.skip_words = skip_words
    data_buff = np.fromfile(data_fname, dtype=np.int32, count=-1, sep='')
    self.rawsamples = int(math.floor((np.size(data_buff, 0) - 0) / (1 + self.adc_channels + 1 + self.pwm_channels + self.dmem_words)))
    self.samples = self.rawsamples - self.filtspan
    if samples is not None:
      if self.samples > samples:
        self.samples = samples
    self.pwm = np.ndarray(shape=(self.pwm_channels, self.rawsamples), dtype=np.int32)
    self.adc_data = np.ndarray(shape=(self.adc_channels, self.samples), dtype=float)
    self.adc_raw = np.ndarray(shape=(self.adc_channels, self.rawsamples), dtype=np.int32)
    self.adc_sqn = np.ndarray(shape=(self.rawsamples), dtype=np.uint32)
    self.adc_offset = np.ndarray(shape=(self.adc_channels), dtype=float)
    self.current = np.ndarray(shape=(self.adc_channels, self.samples), dtype=float)
    self.cur_alpbet = np.ndarray(shape=(2, self.samples), dtype=float)

    self.pwm_d = np.ndarray(shape=(1, self.samples), dtype=float)
    self.pwm_q = np.ndarray(shape=(1, self.samples), dtype=float)
    self.cur_d = np.ndarray(shape=(1, self.samples), dtype=float)
    self.cur_q = np.ndarray(shape=(1, self.samples), dtype=float)
    self.ptindx = np.ndarray(shape=(1, self.samples), dtype=np.uint32)
    self.ptphs = np.ndarray(shape=(1, self.samples), dtype=np.int32)

    self.adc_offset[0] = 2070.881306
    self.adc_offset[1] = 2073.552682
    self.adc_offset[2] = 2050.669577

    self.curcal = np.array(
        [[ 0.0063098,  -0.00182591,  0.00580102],
         [ 0.00245373,  0.00224583, -0.00493571],
         [-0.00876353, -0.00041991, -0.00086531]]
      ) * 1000

    sample_pos = 0;
    for i in range(0, self.rawsamples):
      pos = sample_pos + self.skip_words
      if i < self.samples:
        self.ptindx[0, i] = data_buff[pos];
      pos += 1
      for ch in range(0, self.pwm_channels):
        self.pwm[ch, i] = data_buff[pos] & 0x3fff
        pos += 1
      self.adc_sqn[i] = data_buff[pos];
      pos += 1
      for ch in range(0, self.adc_channels):
        self.adc_raw[ch, i] = data_buff[pos]
        pos += 1
      for a in range(0, self.dmem_words):
        self.dmem[a, i] = data_buff[pos]
        pos += 1
      sample_pos = pos

    nominal_cycle = self.period * self.adc_oversample;
    for ch in range(0, self.adc_channels):
      for i in range(0, self.samples):
        #cycle_time = self.adc_raw[ch, i + self.filtspan] + nominal_cycle  * self.filtspan - self.adc_raw[ch, i]
        #cycle_time = nominal_cycle * self.filtspan
        fac = 1.0
        sampsum = 0.0
        for j in range(0, self.filtspan):
          sqn_inc = float(self.adc_sqn[i]);
          if sqn_inc < 1:
            sampsum += 0
          else:
            sampsum += float(self.adc_raw[ch, i + j]) / sqn_inc
        self.adc_data[ch, i] = sampsum * fac
    for ch in range(0, self.adc_channels):
      for i in range(0, self.samples):
        self.current[ch, i] = self.adc_data[ch, i] - self.adc_offset[ch]

    if do_curcal:
      self.current = np.dot(self.curcal, self.current)

    clarke_t = np.array([[1.0, -0.5, -0.5], [0, math.sqrt(3)/2.0, -math.sqrt(3)/2.0]])
    self.cur_alpbet = np.dot(clarke_t, self.current)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('-f', '--filtspan', dest='filtspan', type=int,
                      default=1, help='number of samples used in moving average')
  parser.add_argument('-p', '--period', dest='period', type=int,
                      default=2500, help='period period of ')
  parser.add_argument('-a', '--adc-oversample', dest='adc_oversample', type=int,
                      default=1, help='ADC oversample over period')
  parser.add_argument('-d', '--isr-divider', dest='isr_divider', type=int,
                      default=1, help='Rx done ISR divider')
  parser.add_argument('-c', '--channels', dest='channels', type=int,
                      default=3, help='number of measured current ADC channels')
  parser.add_argument('-s', '--samples', dest='samples', type=int,
                      default=None, help='limit number of processed samples')
  parser.add_argument('-C', '--curcal', dest='do_curcal', action='store_true',
                      default=False, help='Use current callibration')
  parser.add_argument('files', nargs=argparse.REMAINDER)

  args = parser.parse_args()

  if len(args.files) == 0:
    args.files.append('adc-out.bin')

  adc_data = spimc_state(args.files[0], filtspan = args.filtspan,
                            period = args.period, adc_oversample = args.adc_oversample,
                            isr_divider = args.isr_divider, adc_channels = args.channels,
                            samples = args.samples, pwm_channels = args.channels,
                            do_curcal = args.do_curcal)

  x = range(0, adc_data.samples)
  xr = range(0, adc_data.rawsamples)
  xf = range(adc_data.filtspan + 0, adc_data.samples + adc_data.filtspan)
  y = adc_data.ptindx[0]
  y1 = adc_data.pwm[0]
  y2 = adc_data.pwm[1]
  y3 = adc_data.pwm[2]
  #y4 = adc_data.adc_data[0]
  #y5 = adc_data.adc_data[1]
  #y6 = adc_data.adc_data[2]
  y4 = adc_data.current[0]
  y5 = adc_data.current[1]
  y6 = adc_data.current[2]
  y7 = adc_data.adc_sqn
  y8 = y4 + y5 + y6

  cur_alp = adc_data.cur_alpbet[0]
  cur_bet = adc_data.cur_alpbet[1]

  cur_mag = np.sqrt(cur_alp**2 + cur_bet**2)
  cur_ph = np.arctan2(cur_alp, cur_bet)

  #pwm_ph = np.arctan2(adc_data.pwm_q, adc_data.pwm_d)
  #cur_ph = np.arctan2(adc_data.cur_q_filt, adc_data.cur_d_filt)
  #cur_ph = np.arctan2(adc_data.cur_q, adc_data.cur_d)

  #y7 = cur_ph[0] * 180.0 / math.pi * 10
  #y8 = pwm_ph[0] * 180.0 / math.pi * 10

  #y9 = adc_data.ptphs[0] * 100

  plot(x, y, 'y', xr, y1, 'r.', xr, y2, 'g.', xr, y3, 'b.',
       x, y4, 'r-', x, y5, 'g-', x, y6, 'b-', xr, y7, 'k',
       x, y8, 'c')

  show()

  plot(x, y, 'k', x, cur_alp, 'm', x, cur_bet, 'c',
       x, cur_mag, 'r', x, cur_ph*3600/2/math.pi, 'g')

  show()
